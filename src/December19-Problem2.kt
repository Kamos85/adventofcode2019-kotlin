// Approach to this problem:
// Get an initial estimation by getting the coefficient of the lower and upper boundaries of the beam and
// then, knowing the diagonal d of a 100x100 square, find a good estimate where the lower and upper boundary are also
// distance d apart.
// Since this gives a solution that is too high (we don't know the internals of the given int-program), build a
// grid in the estimated neighbourhood and do the final fitting in there

package aoc.december19.problem2

import DoubleVector2
import IntVector2
import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File
import java.lang.Exception
import kotlin.math.sqrt

val map : MutableList<MutableList<Char>> = mutableListOf()
const val size = 800 + 1
const val targetSquareSize = 100

var lowerLightCoefficient = 0.0
var upperLightCoefficient = 0.0

fun main()
{
    val input: String = File("input/December19-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    val program = IntCodeComputer(instructionData)

    CalculateCoefficients(program)
    val estimatedAnswer = GetEstimatedAnswer()
    BuildNewMapInEstimatedArea(program, estimatedAnswer)

    val fittedLocalAnswer = DoFitting(targetSquareSize)
    println("fittedLocalAnswer: $fittedLocalAnswer")
    if (fittedLocalAnswer.y == 0)
    {
        println("local fit y was zero, which means the fitting is almost certainly to be with a lower y")
        return
    }

    val finalAnswer = fittedLocalAnswer + IntVector2(estimatedAnswer.x.toInt() - 50, estimatedAnswer.y.toInt() - 50)
    println("answer location: $finalAnswer")
    println("Answer: ${finalAnswer.x*10000+finalAnswer.y}")
}

fun CalculateCoefficients(program : IntCodeComputer)
{
    for (y in 0 until size)
    {
        map.add(mutableListOf())
        for (x in 0 until size)
        {
            program.Reset()
            program.AddInput(x.toLong())
            program.AddInput(y.toLong())
            program.Run()
            val result = program.GetOutput()
            if (result == 0L)
                map.last().add('.')
            else
                map.last().add('#')
        }
    }

    val lastLine = map.last()
    val firstLightOnLastLine = lastLine.indexOfFirst { i -> i == '#' }
    val lastLightOnLastLine = lastLine.indexOfLast { i -> i == '#' }
    lowerLightCoefficient = firstLightOnLastLine.toDouble() / (size-1)
    upperLightCoefficient = lastLightOnLastLine.toDouble() / (size-1)
}

fun GetEstimatedAnswer() : DoubleVector2
{
    var scanLine = 10
    val targetDiagonal = sqrt(targetSquareSize*targetSquareSize*2.0)
    do
    {
        val upperLastLightX = scanLine * upperLightCoefficient
        val lowerFirstLightX = (scanLine+targetSquareSize) * lowerLightCoefficient
        val upperLastLightPos = DoubleVector2(upperLastLightX,scanLine.toDouble())
        val lowerFirstLightPos = DoubleVector2(lowerFirstLightX,(scanLine+targetSquareSize).toDouble())
        val diagonalDistance = sqrt((lowerFirstLightPos - upperLastLightPos).LengthSqr)
        if (diagonalDistance > targetDiagonal)
            return DoubleVector2(lowerFirstLightPos.x, upperLastLightPos.y)
        scanLine++
    } while (true)
}

fun BuildNewMapInEstimatedArea(program : IntCodeComputer, estimatedAnswer : DoubleVector2)
{
    map.clear()
    for (y in estimatedAnswer.y.toInt() - 50 until estimatedAnswer.y.toInt() + targetSquareSize + 5)
    {
        map.add(mutableListOf())
        for (x in estimatedAnswer.x.toInt() - 50 until estimatedAnswer.x.toInt() + targetSquareSize + 5)
        {
            program.Reset()
            program.AddInput(x.toLong())
            program.AddInput(y.toLong())
            program.Run()
            val result = program.GetOutput()
            if (x == estimatedAnswer.x.toInt() && y == estimatedAnswer.y.toInt())
                map.last().add('O')
            else
            {
                if (result == 0L)
                    map.last().add('.')
                else
                    map.last().add('#')
            }
        }
    }
}

fun DoFitting(squareSize : Int) : IntVector2
{
    for (y in 0 until map.size - squareSize)
        for (x in 0 until map.last().size - squareSize)
            if (CheckIfSquareFits(x, y, squareSize))
                return IntVector2(x,y)
    throw Exception("No fit")
}

fun CheckIfSquareFits(px : Int, py : Int, squareSize : Int) : Boolean
{
    for (y in 0 until squareSize)
    {
        if (map[py+y][px] != '#')
            return false
        if (map[py+y][px+squareSize-1] != '#')
            return false
    }
    return true
}

fun PrintScreen()
{
    map.forEach { i -> println(i.joinToString("")) }
}
