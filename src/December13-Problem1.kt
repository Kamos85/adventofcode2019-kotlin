package aoc.december13.problem1

import IntVector2
import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File
import java.lang.Integer.max
import java.lang.Integer.min

enum class Visual(val value: Int, val strRep: String)
{
    Empty(0, ConsoleColors.BLACK+" "),
    Wall(1, ConsoleColors.WHITE_BACKGROUND+"#"),
    Block(2, ConsoleColors.GREEN_BACKGROUND+"*"),
    Paddle(3, ConsoleColors.BLUE_BACKGROUND+"_"),
    Ball(4, ConsoleColors.RED_BACKGROUND+"o")
}

val screenMap = mutableMapOf<IntVector2, Visual>()

fun main()
{
    val input: String = File("input/December13-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    val program = IntCodeComputer(instructionData)

    program.Run()

    do
    {
        val x = program.GetOutput().toInt()
        val y = program.GetOutput().toInt()
        val type = program.GetOutput().toInt()
        UpdateScreen(IntVector2(x,y), type)
    } while (program.HasOutput())

    PrintScreen()

    var countBlocks = 0;
    screenMap.forEach {
        i -> if (i.value == Visual.Block) countBlocks++
    }

    println("Answer: " + countBlocks)
}

fun PrintScreen()
{
    var minx = 9999
    var maxx = -9999
    var miny = 9999
    var maxy = -9999
    screenMap.forEach {
        i ->
        run {
            minx = min(minx, i.key.x)
            maxx = max(maxx, i.key.x)
            miny = min(miny, i.key.y)
            maxy = max(maxy, i.key.y)
        }
    }

    for (y in miny..maxy)
    {
        for (x in minx..maxx)
        {
            if (screenMap.containsKey(IntVector2(x, y)))
                print(screenMap[IntVector2(x,y)]?.strRep)
            else
                print(Visual.Empty.strRep)
        }
        print(ConsoleColors.BLACK)
        println()
    }
}

fun UpdateScreen(pos : IntVector2, type : Int)
{
    var visual = when (type)
    {
        Visual.Empty.value -> Visual.Empty
        Visual.Wall.value -> Visual.Wall
        Visual.Block.value -> Visual.Block
        Visual.Paddle.value -> Visual.Paddle
        Visual.Ball.value -> Visual.Ball
        else -> throw Exception("Unknown visual type: $type")
    }
    screenMap[pos] = visual
}
