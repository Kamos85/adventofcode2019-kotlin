// Approach to this problem:
// First make a step and see what the response from the bot is
// Then build a crawler and see what we encounter
// Saw there is a pressure plate that teleports you to the checkpoint, that messes with the crawler
// Changed crawler so it would not go beyond the checkpoint
// Changed crawler to pick up all items it finds
// Changed crawler to not pick up the molten lava as it stops the bot from working
// Changed crawler to not pick up the photons as it stops the bot from working
// Changed crawler to not pick up the giant electromagnet as it stops the bot from working
// Changed crawler to not pick up the escape pod as it stops the bot from working
// Did not have enough weight for the sensor but no more items could be picked up
// Tried leaving items in rooms to see if they active anything
// Mapped out the rooms
// Saw that doing discovery by position will skip some rooms and rooms can be above each other
// For example: the Hallway and Crew Quarters are on the same 2D position
// Changed crawler to do discover by visited rooms and not by position
// Found a couple more rooms, taken more items
// Go to security room
// Go through all combinations of items over the pressure plate

package aoc.december25.problem1

import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File

enum class Direction(val value: String)
{
    North("north"),
    South("south"),
    West("west"),
    East("east");

    companion object
    {
        fun Opposite(direction : Direction) : Direction
        {
            return when (direction)
            {
                North -> South
                South -> North
                West -> East
                East -> West
            }
        }
    }
}

val directions = listOf(Direction.North, Direction.South, Direction.West, Direction.East)
var program = IntCodeComputer(mutableListOf())
val visitedRooms = mutableSetOf<String>()
// these item stop the bot from working, don't take them
val dontTakeItems = mutableSetOf("molten lava", "photons", "giant electromagnet", "escape pod", "infinite loop")
val takenItems = mutableSetOf<String>()

fun main()
{
    val input: String = File("input/December25-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    program = IntCodeComputer(instructionData)
    program.Run()
    println(GetProgramOutput())

    Crawl("Hull Breach")
    GoToSecurityCheckpoint()

    println(RunCommand("inv"))
    GetPastPressureFloorWithItems(takenItems.toMutableList())
}

fun GetPastPressureFloorWithItems(currentItems : MutableList<String>)
{
    var selector = (1 shl currentItems.size) - 1

    while (selector >= 0)
    {
        for (itemIndex in 0 until currentItems.size)
        {
            if ((selector and (1 shl itemIndex)) > 0)
                (RunCommand("take ${currentItems[itemIndex]}"))
            else
                (RunCommand("drop ${currentItems[itemIndex]}"))
        }
        selector--
        val result = RunCommand("west")
        if (!result.contains("ejected back"))
        {
            println(result)
            return;
        }
    }
}

fun GoToSecurityCheckpoint()
{
    println("> Going to security checkpoint")
    println(RunCommand("west"))
    println(RunCommand("south"))
    println(RunCommand("west"))
}

fun Crawl(room : String)
{
    visitedRooms.add(room)

    for (direction in directions)
    {
        var goBack = false
        val result = RunCommand(direction.value)
        if (result.contains("You can't go that way."))
            continue

        println("> GOING ${direction.value}")

        val firstEqualsChar = result.indexOfFirst { i -> i == '=' }
        val lastEqualsChar = result.indexOfLast { i -> i == '=' }
        val newRoom = result.substring(firstEqualsChar + 3, lastEqualsChar - 1)
        if (visitedRooms.contains(newRoom))
            goBack = true

        println(result)

        if (result.contains("Security Checkpoint"))
        { // immediately go back
            goBack = true
            visitedRooms.add(newRoom)
        }

        if (goBack)
        {
            println("> ALREADY VISITED OR SECURITY CHECKPOINT, GOING BACK")
            println(RunCommand(Direction.Opposite(direction).value))
            continue
        }

        if (result.contains("Items here:"))
        {
            val index = result.indexOf("Items here:")
            var itemIndex = index + "Items here:".length+3
            var itemName = ""
            while (result[itemIndex].toInt() != 10)
            {
                itemName += result[itemIndex]
                itemIndex++
            }

            if (!dontTakeItems.contains(itemName))
                TakeItem(itemName)
        }


        Crawl(newRoom)
        println("> BACK TRACK")
        println(RunCommand(Direction.Opposite(direction).value))
    }
}

fun TakeItem(itemName : String)
{
    println(RunCommand("take $itemName"))
    takenItems.add(itemName)
}

fun RunCommand(command : String) : String
{
    SendStringToProgram(command)
    program.Run()
    return GetProgramOutput()
}

fun SendStringToProgram(command : String)
{
    for (c in command)
        program.AddInput(c.toLong())
    program.AddInput(10)
}

fun GetProgramOutput() : String
{
    var programOutput = ""
    while (program.HasOutput())
    {
        val output = program.GetOutput()
        if (output > 255)
            programOutput += output
        else
            programOutput += output.toChar()
    }
    return programOutput
}
