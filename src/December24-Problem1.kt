// Approach to this problem:
// Use a list of lists to do the simulation
// Before doing a simulation step copy the old state and use that to compute the new state
// Calculate the biodiversity rating as requested

package aoc.december24.problem1

import java.io.File

val map : MutableList<MutableList<Char>> = mutableListOf()
var mapHeight = -1
var mapWidth = -1

fun main()
{
    ParseMap()

    val seenRatings = mutableSetOf<Int>()
    var biodiversityRating = -1
    var steps = 0
    while (true)
    {
        biodiversityRating = CalculateBiodiversityRating()
        if (seenRatings.contains(biodiversityRating))
            break
        seenRatings.add(biodiversityRating)
        DoSimulationStep()
        steps++
    }

    PrintMap()
    println("Answer: $biodiversityRating $steps")
}

fun ParseMap()
{
    val input = File("input/December24-Problem.txt").readLines()
    for (i in input)
        map.add(i.toCharArray().toMutableList())
    mapHeight = map.size
    mapWidth = map.last().size
}

fun DoSimulationStep()
{
    val oldMap : MutableList<MutableList<Char>> = mutableListOf()
    for (row in map)
        oldMap.add(row.toMutableList())

    var bugNeighbours = 0
    for (y in 0 until mapHeight)
    {
        for (x in 0 until mapWidth)
        {
            bugNeighbours = GetBugNeighboursInMapForPosition(oldMap, x, y)

            if (oldMap[y][x] == '#')
            {
                if (bugNeighbours != 1)
                    map[y][x] = '.' // bug dies
            }
            else
            {
                if (bugNeighbours in 1..2)
                    map[y][x] = '#' // bug born
            }
        }
    }
}

fun GetBugNeighboursInMapForPosition(aMap : MutableList<MutableList<Char>>, x : Int, y : Int) : Int
{
    var bugNeighbours = 0
    if (y-1 >= 0)
        if (aMap[y-1][x] == '#')
            bugNeighbours++
    if (y+1 < mapHeight)
        if (aMap[y+1][x] == '#')
            bugNeighbours++
    if (x-1 >= 0)
        if (aMap[y][x-1] == '#')
            bugNeighbours++
    if (x+1 < mapWidth)
        if (aMap[y][x+1] == '#')
            bugNeighbours++

    return bugNeighbours
}

fun CalculateBiodiversityRating() : Int
{
    var biodiversityRating = 0
    for (y in mapHeight-1 downTo 0)
        for (x in mapWidth-1 downTo 0)
        {
            if (map[y][x] == '#')
                biodiversityRating = biodiversityRating or 1
            biodiversityRating = biodiversityRating shl 1
        }
    biodiversityRating = biodiversityRating shr 1  // shifted one too many in the loop
    return biodiversityRating
}


fun PrintMap()
{
    for (row in map)
        println(row.joinToString(""))
}
