// Approach to this problem:
// Go over list, apply calculation to elements using a recursive function to also compute the mass of fuel
// and return the sum

package aoc.december1.problem2

import java.io.File

fun main()
{
    val inputLines: List<String> = File("input/December01-Problem.txt").readLines()
    val inputLinesFuelRequired = inputLines.map { calculateFuelForMass(it.toInt()) }
    val answer = inputLinesFuelRequired.sum()
    println("Answer: $answer")
}

fun calculateFuelForMass(mass : Int) : Int
{
    val fuel = mass / 3 - 2
    if (fuel < 0)
        return 0;
    else
        return fuel + calculateFuelForMass(fuel)
}
