package aoc.december16.problem1

import java.io.File

const val phaseCount = 100
val basePattern = listOf(0, 1, 0, -1)

class SquareMatrix(val size : Int)
{
    var entries : MutableList<MutableList<Int>> = mutableListOf()
    init
    {
        val row = mutableListOf<Int>()
        for (i in 0 until size)
            row.add(0)
        for (i in 0 until size)
            entries.add(row.toMutableList())
    }

    override fun toString(): String
    {
        var result = ""
        for (i in entries)
            result += i.toString() + "\n"
        return result;
    }

    fun Set(x : Int, y : Int, value : Int)
    {
        entries[y][x] = value
    }

    fun Multiply(ciphers : List<Int>) : List<Int>
    {
        val result = ciphers.toMutableList()
        for (y in 0 until size)
        {
            var sum = 0
            for (x in 0 until size)
                sum += entries[y][x] * ciphers[x]
            result[y] = kotlin.math.abs((sum % 10))
        }
        return result
    }
}

fun main()
{
    val input = File("input/December16-Problem.txt").readText()
    var ciphers = (input.map { i -> i.toString().toInt() }).toList()
    val multMatrix = BuildMultiplicationMatrix(ciphers.size)

    for (i in 1..phaseCount)
        ciphers = multMatrix.Multiply(ciphers)

    println("Answer: ${ciphers.joinToString("").subSequence(0,8)}")
}

fun BuildMultiplicationMatrix(size : Int) : SquareMatrix
{
    val result = SquareMatrix(size)
    var repeats = 0
    for (repition in 1..size)
    {
        repeats = repition - 1
        var index = 0
        var patternIndex = 0
        do
        {
            if (repeats == 0)
            {
                repeats = repition
                patternIndex = (patternIndex+1) % basePattern.size
            }
            repeats--

            var value = basePattern[patternIndex]
            result.Set(index, repition-1, value)

            index++
        } while (index < size)
    }
    return result
}
