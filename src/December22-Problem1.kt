// Approach to this problem:
// Use a list to represent the deck and build the required operations

package aoc.december22.problem1

import java.io.File

var cardDeck : MutableList<Int> = mutableListOf()

fun main()
{
    for (i in 0..10006)
        cardDeck.add(i)

    println(cardDeck)
    val input = File("input/December22-Problem.txt").readLines()
    for (line in input)
    {
        if (line.startsWith("cut"))
        {
            CutDeck(line.substring(4).toInt())
        } else
        {
            if (line.startsWith("deal into"))
            {
                DealIntoNewStack()
            } else
            {
                if (!line.startsWith("deal with"))
                    throw Exception("unknown line parse: $line")
                DealWithN(line.substring(20).toInt())
            }
        }
    }
    println(cardDeck)

    val card2019Position = cardDeck.indexOf(2019)
    println("Answer: $card2019Position")
}

fun CutDeck(cut : Int)
{
    if (cut > 0)
    {
        for (i in 0 until cut)
        {
            val removedCard = cardDeck.removeAt(0)
            cardDeck.add(removedCard)
        }
    } else
    {
        for (i in 0 until -cut)
        {
            val removedCard = cardDeck.removeAt(cardDeck.size-1)
            cardDeck.add(0, removedCard)
        }
    }
}

fun DealIntoNewStack()
{
    cardDeck.reverse()
}

fun DealWithN(n : Int)
{
    val newDeck = cardDeck.toMutableList()
    var step = 0
    for (i in 0 until cardDeck.size)
    {
        newDeck[step % cardDeck.size] = cardDeck[i]
        step += n
    }
    cardDeck = newDeck
}
