// Approach to this problem:
// Simply follow the instructions
// Have 50 instances of the program and have a bit of code that handles the sending/receiving of data between programs

package aoc.december23.problem1

import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File

val computers : MutableList<IntCodeComputer> = mutableListOf()
const val computerCount = 50

fun main()
{
    val input: String = File("input/December23-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    for (i in 0 until computerCount)
    {
        computers.add(IntCodeComputer(instructionData))
        computers.last().AddInput(i.toLong())
    }

    for (runs in 1..200)
    {
        for (computer in computers)
        {
            computer.Run()
            if (!computer.IsComplete())
                computer.AddInput(-1)
            while (computer.HasOutput())
            {
                val address = computer.GetOutput().toInt()
                val x = computer.GetOutput()
                val y = computer.GetOutput()
                if (address == 255)
                {
                    println("address: $address , x: $x , y: $y")
                }
                else
                {
                    computers[address].AddInput(x)
                    computers[address].AddInput(y)
                }
            }
        }
    }
}
