package aoc.december6.problem1

import java.io.File

val keyOrbitsValueMap : MutableMap<String, String> = mutableMapOf()
val orbitCountMap : MutableMap<String, Int> = mutableMapOf()

fun main()
{
    val input = File("input/December06-Problem.txt").readLines()
    val parseRegex = Regex("(?<A>.+)\\)(?<B>.+)")

    for (i in input)
    {
        val groups = parseRegex.find(i)?.groups
        val A = groups?.get("A")?.value.orEmpty()
        val B = groups?.get("B")?.value.orEmpty()
        keyOrbitsValueMap[B] = A
    }

    for (node in keyOrbitsValueMap)
        ProcessTotalOrbitCountFor(node.key)

    var sum = 0
    for (node in orbitCountMap)
        sum += node.value

    println("Answer: $sum")
}

fun ProcessTotalOrbitCountFor(node : String)
{
    if (orbitCountMap.containsKey(node))
        return // already processed

    val center = keyOrbitsValueMap.getOrElse(node) {null}
    if (center.isNullOrEmpty())
    {
        orbitCountMap[node] = 0
        return
    }

    ProcessTotalOrbitCountFor(center)
    val orbitCount =  1 + orbitCountMap.getOrDefault(center, 0)
    orbitCountMap[node] = orbitCount
}
