package aoc.december9.intcodecomputer

enum class Instruction(val value: Int)
{
    ADD(1),
    MULTIPLY(2),
    INPUT(3),
    OUTPUT(4),
    JUMP_IF_TRUE(5),
    JUMP_IF_FALSE(6),
    LESS_THAN(7),
    EQUALS(8),
    ADJUST_RELATIVE_BASE(9),
    STOP(99)
}

enum class ParameterMode(val value: Int)
{
    POSITION(0),
    IMMEDIATE(1),
    RELATIVE(2)
}

class IntCodeComputer(var initialInstructionData: MutableList<Long>)
{
    var instructionPointer = 0
    var inputStream = mutableListOf<Long>()
    var outputStream = mutableListOf<Long>()
    var relativeBase  = 0

    var memory = initialInstructionData.toMutableList()

    val parameterModes = mutableListOf(0,0,0)

    fun Run(withExceptions : Boolean = false)
    {
        if (withExceptions)
        {
            try
            {
                while (HandleInstruction())
                    ;
            }
            catch (e : Exception)
            {
                val opcodeWithParameterModes = memory[instructionPointer]
                val opcode = opcodeWithParameterModes % 100
                throw Exception("opcode $opcode [@instruction: $instructionPointer] threw exception. Original exception: " + e.message)
            }
        } else
        {
            while (HandleInstruction())
                ;
        }
    }

    fun IsComplete() : Boolean
    {
        val opcodeWithParameterModes = memory[instructionPointer]
        val opcode = opcodeWithParameterModes % 100
        return opcode.toInt() == Instruction.STOP.value
    }

    fun Reset()
    {
        memory = initialInstructionData.toMutableList()

        instructionPointer = 0
        inputStream = mutableListOf<Long>()
        outputStream = mutableListOf<Long>()
        relativeBase  = 0
    }

    fun AddInput(vararg input: Long)
    {
        inputStream.addAll(input.toList())
    }

    fun HasOutput() : Boolean
    {
        return outputStream.size > 0
    }

    fun GetOutput() : Long
    {
        return outputStream.removeAt(0)
    }

    fun HandleInstruction(): Boolean
    {
        val opcodeWithParameterModes = memory[instructionPointer]
        val opcode = (opcodeWithParameterModes % 100).toInt()
        val parameterModes = opcodeWithParameterModes / 100
        SetParameterModes(parameterModes.toInt())

        when (opcode)
        {
            Instruction.ADD.value -> Instruction_Add()
            Instruction.MULTIPLY.value -> Instruction_Multiply()
            Instruction.INPUT.value -> return Instruction_Input() // can halt when there is no input
            Instruction.OUTPUT.value -> Instruction_Output()
            Instruction.JUMP_IF_TRUE.value -> Instruction_Jump_If_True()
            Instruction.JUMP_IF_FALSE.value -> Instruction_Jump_If_False()
            Instruction.LESS_THAN.value -> Instruction_Less_Than()
            Instruction.EQUALS.value -> Instruction_Equals()
            Instruction.ADJUST_RELATIVE_BASE.value -> Instruction_Adjust_Relative_Base()
            Instruction.STOP.value -> return false
            else -> println("Unknown instruction: $opcode")
        }
        return true
    }

    fun EnsureMemoryIndexExists(memoryIndex : Int)
    {
        while (memoryIndex+1 > memory.size)
            memory.add(0)
    }

    fun StoreInMemory(memoryIndex: Int, value : Long)
    {
        EnsureMemoryIndexExists(memoryIndex)
        memory[memoryIndex] = value
    }

    fun ReadFromMemory(memoryIndex: Int) : Long
    {
        EnsureMemoryIndexExists(memoryIndex)
        return memory[memoryIndex]
    }

    fun GetValueFromParameter(parameterIndex: Int): Long
    {
        val parameterMode = parameterModes[parameterIndex - 1]
        val operandValue = memory[instructionPointer + parameterIndex]

        return when (parameterMode)
        {
            ParameterMode.POSITION.value -> ReadFromMemory(operandValue.toInt())
            ParameterMode.IMMEDIATE.value -> operandValue
            ParameterMode.RELATIVE.value -> ReadFromMemory(relativeBase + operandValue.toInt())
            else -> throw IndexOutOfBoundsException("Invalid parameter mode: $parameterMode")
        }
    }

    fun StoreValueAtParameter(parameterIndex : Int, value : Long)
    {
        val parameterMode = parameterModes[parameterIndex - 1]
        val operandValue = memory[instructionPointer + parameterIndex]

        var memoryIndex = operandValue.toInt()
        if (parameterMode == ParameterMode.RELATIVE.value)
            memoryIndex += relativeBase

        StoreInMemory(memoryIndex, value)
    }

    fun GetParameterModeForParameter(parameterIndex : Int) : Int
    {
        return parameterModes[parameterIndex - 1]
    }

    fun Instruction_Add()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        StoreValueAtParameter(3, value1 + value2)
        instructionPointer += 4
    }

    fun Instruction_Multiply()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        StoreValueAtParameter(3, value1 * value2)
        instructionPointer += 4
    }

    fun Instruction_Input() : Boolean
    {
        if (inputStream.isEmpty())
            return false; // halt
        StoreValueAtParameter(1, inputStream.removeAt(0))
        instructionPointer += 2
        return true;
    }

    fun Instruction_Output()
    {
        val value1 = GetValueFromParameter(1)
        outputStream.add(value1)
        instructionPointer += 2
    }

    fun Instruction_Jump_If_True()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        if (value1 != 0L)
            instructionPointer = value2.toInt()
        else
            instructionPointer += 3
    }

    fun Instruction_Jump_If_False()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        if (value1 == 0L)
            instructionPointer = value2.toInt()
        else
            instructionPointer += 3
    }

    fun Instruction_Less_Than()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        StoreValueAtParameter(3, if (value1 < value2) 1 else 0)
        instructionPointer += 4
    }

    fun Instruction_Equals()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        StoreValueAtParameter(3, if (value1 == value2) 1 else 0)
        instructionPointer += 4
    }

    fun Instruction_Adjust_Relative_Base()
    {
        val value1 = GetValueFromParameter(1)
        relativeBase += value1.toInt()
        instructionPointer += 2
    }

    fun SetParameterModes(parameters: Int)
    {
        ResetParameterModes()
        var params = parameters
        var index = 0
        while (params > 0) {
            parameterModes[index] = params % 10
            params /= 10
            index++
        }
    }

    fun ResetParameterModes()
    {
        for (i in parameterModes.indices)
            parameterModes[i] = 0
    }
}
