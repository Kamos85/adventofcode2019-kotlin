// Approach to this problem:
// simply running the program on a 50x50 grid and counting how many tiles have pull

package aoc.december19.problem1

import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File

val map : MutableList<MutableList<Char>> = mutableListOf()
const val size = 50

fun main()
{
    val input: String = File("input/December19-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    val program = IntCodeComputer(instructionData)

    var sum = 0
    for (y in 0 until size)
    {
        map.add(mutableListOf())
        for (x in 0 until size)
        {
            program.Reset()
            program.AddInput(x.toLong())
            program.AddInput(y.toLong())
            program.Run()
            val result = program.GetOutput()
            if (result == 0L)
            {
                map.last().add('.')
            }
            else
            {
                map.last().add('#')
                sum++
            }
        }
    }
    PrintScreen()
    println("Answer: $sum")
}

fun PrintScreen()
{
    map.forEach { i -> println(i.joinToString("")) }
}
