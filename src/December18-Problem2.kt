package aoc.december18.problem2

import aoc.december18.MazeRobot.MazeRobot
import aoc.december18.nodeandtransition.Node
import java.io.File
import java.lang.Exception
import java.lang.Integer.min

val map : MutableList<MutableList<Char>> = mutableListOf()
val cache : MutableMap<MutableSet<String>, Int> = mutableMapOf()

val mazeRobots : MutableList<MazeRobot> = mutableListOf()

fun main()
{
    ParseMap()

    val startNodes = mutableListOf<Node>()
    for (mazeRobot in mazeRobots)
        startNodes.add(mazeRobot.GetStartNode())

    val minSteps = GetNextKey(startNodes, mutableSetOf())
    println("Answer: $minSteps")
}

fun ParseMap()
{
    val input = File("input/December18-Day-2-Problem.txt").readLines()

    for (i in 0 until 4)
    {
        val mazeRobot = MazeRobot()
        mazeRobot.Setup(i, input)
        mazeRobots.add(mazeRobot)
    }
}

fun GetNextKey(robotPositions : MutableList<Node>, collectedKeys : MutableSet<String>) : Int
{
    val accessibleUncollectedKeys = GetDirectUncollectedAccessibleKeys(collectedKeys)

    if (accessibleUncollectedKeys.size == 0)
        return 0

    val cacheKey = accessibleUncollectedKeys.toMutableSet()
    for (i in 0 until robotPositions.size)
        cacheKey.add(i.toString() + robotPositions[i].key)
    val cacheResult = cache[cacheKey]
    if (cacheResult != null)
        return cacheResult

    var minSteps = Int.MAX_VALUE
    for (key in accessibleUncollectedKeys)
    {
        val robotToMoveIndex = GetMazeRobotIndexToGetKey(key)
        val robotToMove = mazeRobots[robotToMoveIndex]
        val robotStartNode = robotPositions[robotToMoveIndex]
        val robotTargetNode = robotToMove.GetNodeForKey(key)?:throw Exception("Key $key has no node")
        val distanceToKey = robotToMove.GetStepsToNode(robotStartNode, robotTargetNode)

        robotPositions[robotToMoveIndex] = robotTargetNode
        collectedKeys.add(key)
        val totalSteps = distanceToKey + GetNextKey(robotPositions, collectedKeys)
        minSteps = min(minSteps, totalSteps)
        collectedKeys.remove(key)
        robotPositions[robotToMoveIndex] = robotStartNode
    }

    cache[cacheKey] = minSteps

    return minSteps
}

fun GetMazeRobotIndexToGetKey(key : String) : Int
{
    for (i in 0 until mazeRobots.size)
        if (mazeRobots[i].GetNodeForKey(key) != null)
            return i
    throw Exception("No robot can get key $key")
}

fun GetDirectUncollectedAccessibleKeys(collectedKeys: MutableSet<String>): MutableSet<String>
{
    val accessibleKeys = mutableSetOf<String>()
    for (robot in mazeRobots)
        accessibleKeys.addAll(robot.GetDirectUncollectedAccessibleKeys(collectedKeys))
    return accessibleKeys
}
