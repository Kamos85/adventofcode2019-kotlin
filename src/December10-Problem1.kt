package aoc.december10.problem1

import IntVector2
import java.io.File
import kotlin.math.*

val asteroidField : MutableList<MutableList<Int>> = mutableListOf()

fun main()
{
    Setup()

    var maxAsteroidsSeen = -1
    var maxAsteroidsSeenXYPos = IntVector2(-1, -1)
    for (y in 0 until asteroidField.size)
    {
        for (x in 0 until asteroidField.first().size)
        {
            if (asteroidField[y][x] == 0)
                continue
            val asteroidsSeen = GetVisibleAsteroidsFromAsteroid(x, y)
            if (maxAsteroidsSeen < asteroidsSeen)
            {
                maxAsteroidsSeen = asteroidsSeen
                maxAsteroidsSeenXYPos.x = x
                maxAsteroidsSeenXYPos.y = y
            }
        }
    }

    println("Answer: $maxAsteroidsSeen at position $maxAsteroidsSeenXYPos")
}

fun Setup()
{
    val input = File("input/December10-Problem.txt").readLines()
    for (line in input)
    {
        asteroidField.add(mutableListOf())
        for (char in line)
            asteroidField.last().add(if (char == '.') 0 else 1)
    }
}

fun GetVisibleAsteroidsFromAsteroid(asteroidX : Int, asteroidY : Int) : Int
{
    val angleHashSet = mutableSetOf<Int>()
    for (y in 0 until asteroidField.size)
    {
        for (x in 0 until asteroidField.first().size)
        {
            if (asteroidField[y][x] == 0)
                continue // empty space
            if (x == asteroidX && y == asteroidY)
                continue // skip own position

            val angle = GetAngleFromNorthToP1P2(IntVector2(asteroidX, asteroidY), IntVector2(x, y))
            val IntAngleTimes100 = (angle * 100).toInt() // prevent math rounding errors
            if (!angleHashSet.contains(IntAngleTimes100))
                angleHashSet.add(IntAngleTimes100)
        }
    }
    return angleHashSet.size
}

fun GetAngleFromNorthToP1P2(p1 : IntVector2, p2 : IntVector2) : Double
{
    val diffX = p2.x - p1.x
    val diffY = p2.y - p1.y
    return atan2(diffX.toDouble(), diffY.toDouble()) * -180 / Math.PI + 180
}
