package aoc.december7.problem2

import aoc.december7.intcodecomputer.IntCodeComputer
import permute
import java.io.File

var amplifiers : MutableList<IntCodeComputer> = mutableListOf()

fun main()
{
    Setup()
    val permutations = permute(listOf(5, 6, 7, 8, 9))
    val maxOutput = permutations.fold(-1, {max, permutation -> kotlin.math.max(max, Run(permutation)) })
    println("Answer: $maxOutput")
}

fun Setup()
{
    val input: String = File("input/December07-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toInt() }.toMutableList()
    for (i in 1..5)
        amplifiers.add(IntCodeComputer(instructionData))
}

fun Run(permutation : List<Int>) : Int
{
    var output = 0
    amplifiers.forEach{i -> i.Reset()}
    for ((index, amplifier) in amplifiers.withIndex())
        amplifier.AddInput(permutation[index])

    do
    {
        for (amplifier in amplifiers)
        {
            amplifier.AddInput(output)
            amplifier.Run()
            output = amplifier.GetOutput()
        }
    }
    while (!amplifiers.last().IsComplete())

    return output
}
