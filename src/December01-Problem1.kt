// Approach to this problem:
// Go over list, apply calculation to elements and return the sum

package aoc.december1.problem1

import java.io.File

fun main()
{
    val inputLines: List<String> = File("input/December01-Problem.txt").readLines()
    val inputLinesFuelRequired = inputLines.map { (it.toInt())/3-2 }
    val answer = inputLinesFuelRequired.sum()
    println("Answer: $answer")
}
