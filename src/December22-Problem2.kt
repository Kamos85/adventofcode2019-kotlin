// Approach to this problem:
// Obviously the input is too big to simulate the described shuffling process
// But only the card at position 2020 is important
// I calculate backwards to find out which card in the beginning input ends up on position 2020
// I was expecting to find a loop, but never found one
// After getting stuck I decided to look for hints and it turns out the solution is quite mathematical, seeing as my
// math is not that great I read some of the solutions. But those turned out to be quite cryptic (in mathematical terms)
// and it was still unclear to me how to solve.
// Finally found a post of someone who was able to explain it:
// https://www.reddit.com/r/adventofcode/comments/ee0rqi/2019_day_22_solutions/fbnifwk/
// and implemented this solution
// The gist of it is:
// - each described instruction can be written as a linear function: ax+b which maps a given index to a new index
// - the shuffle process is a composition of these linear functions and thus also linear and can be written as Ax+B
// - apply mathematics to obtain A and B for the shuffle function
// - apply more modulo exponentiation mathematics to perform the shuffle function the required amount of times

package aoc.december22.problem2

import java.io.File
import java.math.BigInteger

var input : MutableList<String> = mutableListOf()
var inputAction : MutableList<Int> = mutableListOf()
var inputParameter : MutableList<Int> = mutableListOf()

val cards = 119315717514047L // 10007L  //119315717514047L
val shuffles = 101741582076661

fun main()
{
    input = File("input/December22-Problem.txt").readLines().toMutableList()
    input.reverse()
    DoProcess()
    val X = 2020L
    val Y = DoShuffle(X)
    val Z = DoShuffle(Y)

    val bigX = BigInteger.valueOf(X)
    val bigY = BigInteger.valueOf(Y)
    val bigZ = BigInteger.valueOf(Z)
    val bigCards = BigInteger.valueOf(cards)
    val A = ((bigY-bigZ) * (bigX-bigY+bigCards).modInverse(bigCards)).mod(bigCards)
    val B = (bigY-A*bigX).mod(bigCards)
    val bigShuffles = BigInteger.valueOf(shuffles)

    val modPowAToShufflesModCards = A.modPow(bigShuffles, bigCards)
    //(pow(A, n, D)*X + (pow(A, n, D)-1) * modinv(A-1, D) * B) % D
    val answer = ((modPowAToShufflesModCards * bigX) +
            (modPowAToShufflesModCards - BigInteger.valueOf(1)) * (A-BigInteger.valueOf(1)).modInverse(bigCards) * B).mod(bigCards)
    println("Answer: $answer")
}

fun DoProcess()
{
    for (line in input)
    {
        if (line.startsWith("cut"))
        {
            inputAction.add(0)
            inputParameter.add(line.substring(4).toInt())
        } else
        {
            if (line.startsWith("deal into"))
            {
                inputAction.add(1)
                inputParameter.add(0)
            } else
            {
                if (!line.startsWith("deal with"))
                    throw Exception("unknown line parse: $line")
                inputAction.add(2)
                inputParameter.add(line.substring(20).toInt())
            }
        }
    }
}

fun DoShuffle(cardIndex : Long) : Long
{
    var newcardIndex = cardIndex
    for (i in 0 until inputAction.size)
    {
        newcardIndex = when (inputAction[i])
        {
            0 -> ReverseCutDeck(newcardIndex, inputParameter[i])
            1 -> ReverseDealIntoNewStack(newcardIndex)
            2 -> ReverseDealWithN(newcardIndex, inputParameter[i].toLong())
            else -> throw Exception("Should not happen")
        }
    }
    return newcardIndex
}

fun ReverseCutDeck(cardIndex : Long, cut : Int) : Long
{
    return (cardIndex + cut + cards) % cards
}

fun ReverseDealIntoNewStack(cardIndex : Long) : Long
{
    return cards - 1L - cardIndex
}

fun ReverseDealWithN(cardIndex : Long, n : Long) : Long
{
    var newCardIndex = cardIndex
    while ((newCardIndex % n) > 0)
        newCardIndex += cards
    return newCardIndex / n
}
