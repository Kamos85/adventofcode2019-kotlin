package aoc.december10.problem2

import IntVector2
import java.io.File
import kotlin.math.atan2

val asteroidField : MutableList<MutableList<Int>> = mutableListOf()
val monitoringStationPosition = IntVector2(14,17) // discovered by Problem1
val removedAsteroids : MutableList<IntVector2> = mutableListOf()

fun main()
{
    Setup()

    var prevRemovedAsteroidCount = 0
    do
    {
        prevRemovedAsteroidCount = removedAsteroids.size
        BlastAsteroids()
    } while (prevRemovedAsteroidCount != removedAsteroids.size)

    val removedAsteroid200 = removedAsteroids[200-1]
    println("Answer: " + (removedAsteroid200.x * 100 + removedAsteroid200.y))
}

fun Setup()
{
    val input = File("input/December10-Problem.txt").readLines()
    for (line in input)
    {
        asteroidField.add(mutableListOf())
        for (char in line)
            asteroidField.last().add(if (char == '.') 0 else 1)
    }
}

fun BlastAsteroids()
{
    val visibleAsteroids : MutableList<IntVector2> = GetListOfVisibleAsteroidsFromAsteroid(monitoringStationPosition)
    visibleAsteroids.sortWith(object: Comparator<IntVector2>
        {
            override fun compare(a: IntVector2, b: IntVector2): Int
            {
                val angleA = GetAngleFromNorthToP1P2(monitoringStationPosition, a)
                val angleB = GetAngleFromNorthToP1P2(monitoringStationPosition, b)
                return angleA.compareTo(angleB)
            }
        })
    RemoveAsteroids(visibleAsteroids)
}

fun GetAngleFromNorthToP1P2(p1 : IntVector2, p2 : IntVector2) : Double
{
    val diffX = p2.x - p1.x
    val diffY = p2.y - p1.y
    return atan2(diffX.toDouble(), diffY.toDouble()) * -180 / Math.PI + 180
}

fun RemoveAsteroids(asteroidPositions : List<IntVector2>)
{
    asteroidPositions.forEach { i -> asteroidField[i.y][i.x] = 0 }
    removedAsteroids.addAll(asteroidPositions)
}

fun GetListOfVisibleAsteroidsFromAsteroid(asteroidPosition : IntVector2) : MutableList<IntVector2>
{
    val visibleAsteroids = mutableListOf<IntVector2>()
    val visibleAsteroidAngles = mutableListOf<Int>()
    for (y in 0 until asteroidField.size)
    {
        for (x in 0 until asteroidField.first().size)
        {
            if (asteroidField[y][x] == 0)
                continue // empty space
            if (x == asteroidPosition.x && y == asteroidPosition.y)
                continue // skip own position

            val asteroidXY = IntVector2(x, y)
            val angle = GetAngleFromNorthToP1P2(asteroidPosition, asteroidXY)
            val IntAngleTimes100 = (angle * 100).toInt() // prevent math rounding errors
            if (!visibleAsteroidAngles.contains(IntAngleTimes100))
            {
                visibleAsteroidAngles.add(IntAngleTimes100)
                visibleAsteroids.add(asteroidXY)
            } else
            { // check which one is closer
                val existingIndex = visibleAsteroidAngles.indexOf(IntAngleTimes100)
                val existingLengthSqr = GetLengthSqrToCenter(visibleAsteroids[existingIndex])
                if (existingLengthSqr > GetLengthSqrToCenter(asteroidXY))
                    visibleAsteroids[existingIndex] = asteroidXY
            }
        }
    }
    return visibleAsteroids
}

fun GetLengthSqrToCenter(position : IntVector2) : Int
{
    val diffX = position.x - monitoringStationPosition.x
    val diffY = position.y - monitoringStationPosition.y
    return IntVector2(diffX, diffY).LengthSqr
}
