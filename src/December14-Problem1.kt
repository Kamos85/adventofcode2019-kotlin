package aoc.december14.problem1

import aoc.december14.reaction.Reaction
import java.io.File

var neededOre = 0
val neededIngredients : MutableMap<String, Int> = mutableMapOf()
val reactions : MutableList<Reaction> = mutableListOf()

fun main()
{
    Setup()

    neededIngredients["FUEL"] = 1
    do
    {
        println(neededIngredients)
        val ingr = neededIngredients.entries.find { i -> i.value > 0 }
        if (ingr == null)
            break
        RevertReaction(ingr.key, ingr.value)
    } while (neededIngredients.any { e -> e.value > 0})
    println("Answer: $neededOre")
}

fun Setup()
{
    val input = File("input/December14-Problem.txt").readLines()
    val parseReactionRegex = Regex("(?<Inputs>.+) => (?<OutputAmount>\\d+) (?<OutputProduct>.+)")
    val parseInputTermRegex = Regex("(?<Amount>\\d+) (?<Product>.+)")

    for (i in input)
    {
        val reactionGroups = parseReactionRegex.find(i)?.groups
        val inputs = reactionGroups?.get("Inputs")?.value.orEmpty()
        val outputAmount = reactionGroups?.get("OutputAmount")?.value.orEmpty()
        val outputProduct = reactionGroups?.get("OutputProduct")?.value.orEmpty()
        val inputTerms = inputs.split(",")
        val reaction = Reaction(outputProduct, outputAmount.toInt())
        for (j in inputTerms)
        {
            val inputTermGroups = parseInputTermRegex.find(j)?.groups
            val inputAmount = inputTermGroups?.get("Amount")?.value.orEmpty()
            val inputProduct = inputTermGroups?.get("Product")?.value.orEmpty()
            reaction.AddInput(inputProduct, inputAmount.toInt())
        }
        reactions.add(reaction)
    }
}

fun RevertReaction(outputProduct : String, outputAmount : Int)
{
    val r = reactions.find { i -> i.outputProduct == outputProduct }
    if (r == null)
        throw Exception("There is no reaction for $outputProduct")

    neededIngredients[outputProduct] = neededIngredients.getOrDefault(outputProduct, 0) - r.outputAmount

    r.inputs.forEach { i -> AddToNeededIngredients(i.key, i.value) }
}

fun AddToNeededIngredients(product : String, amount : Int)
{
    if (product == "ORE")
    {
        neededOre += amount
        return
    }

    neededIngredients[product] = amount + neededIngredients.getOrDefault(product, 0)
}
