// Approach to this problem:
// Discover all portals with their adjacent open spot as node
// Connect portals on the same level (but without jumps)
// Create copies of this current setup for as many levels as we need (determined manually)
// Connect portals between different levels (inner portal on level i to outer portal on level i+1)
// Run Dijkstra's shortest path algorithm

package aoc.december20.problem2

import IntVector2
import aoc.december20.nodeandtransition.Node
import java.io.File
import java.util.*

enum class Direction(val value: Int, val Vector2 : IntVector2)
{
    North(1, IntVector2(0,-1)),
    South(2, IntVector2(0,1)),
    West(3, IntVector2(-1,0)),
    East(4, IntVector2(1,0));

    companion object
    {
        fun Opposite(direction : Direction) : Direction
        {
            return when (direction)
            {
                North -> South
                South -> North
                West -> East
                East -> West
            }
        }
    }
}

val map : MutableList<MutableList<Char>> = mutableListOf()
var mapWidth = 0
val positionToNode : MutableMap<IntVector2, Node> = mutableMapOf()
val allNodes : MutableList<Node> = mutableListOf()

fun main()
{
    ParseMap()
    DiscoverPortals()
    DiscoverAndConnectPortals()
    CreateAndConnectDeeperLevels(50)
    InvalidateUnavailablePortals()
    val startNode = allNodes.find { i -> i.name == "AA" && i.level == 0 } ?: throw Exception("There is no start node AA")
    val endNode = allNodes.find { i -> i.name == "ZZ" && i.level == 0 } ?: throw Exception("There is no start node ZZ")
    val answer = GetShortestPath(startNode, endNode)
    PrintScreenWithNodes()
    println("Answer: $answer")
}

fun ParseMap()
{
    val input = File("input/December20-Problem.txt").readLines()
    for (line in input)
    {
        map.add(mutableListOf())
        for (c in line)
            map.last().add(c)
    }
    mapWidth = map.last().size
}

fun DiscoverPortals()
{
    val readPortalPositions : MutableSet<IntVector2> = mutableSetOf()

    for (y in 0 until map.size)
    {
        for (x in 0 until mapWidth)
        {
            val char = map[y][x]
            if (char == ' ' || char == '#' || char == '.')
                continue
            val position = IntVector2(x,y)
            if (readPortalPositions.contains(position))
                continue
            ParseNode(position, readPortalPositions)
        }
    }
}

fun ParseNode(position : IntVector2, readPortalPositions : MutableSet<IntVector2>)
{
    var portalName = map[position.y][position.x].toString()
    var portalPosition = IntVector2(-1,-1)
    readPortalPositions.add(position)
    val horizontalLetter = map[position.y][position.x+1]
    if (horizontalLetter != ' ' && horizontalLetter != '#' && horizontalLetter != '.')
    { // horizontal portal name
        portalName += horizontalLetter
        readPortalPositions.add(position + IntVector2(1,0))

        val left = map[position.y].getOrElse(position.x-1) {' '}
        if (left == '.')
            portalPosition = position + IntVector2(-1, 0)
        else
            portalPosition = position + IntVector2(2, 0)
    } else
    {
        portalName += map[position.y + 1][position.x]
        readPortalPositions.add(position + IntVector2(0,1))

        val top = map.getOrElse(position.y-1) { mutableListOf() }
        if (top.size > 0 && top[position.x] == '.')
            portalPosition = position + IntVector2(0,-1)
        else
            portalPosition = position + IntVector2(0,2)
    }

    val innerPortal = portalPosition.x in 4..mapWidth-4 && portalPosition.y in 4..map.size-4

    positionToNode[portalPosition] = Node(portalPosition, portalName, 0, innerPortal)
}

fun DiscoverAndConnectPortals()
{
    for (i in positionToNode)
        DiscoverAndConnectPortal(i.value)
}

fun DiscoverAndConnectPortal(node : Node)
{
    val visitedPositions : MutableSet<IntVector2> = mutableSetOf()
    var currentPos = node.position
    val pathToStart : Stack<Direction> = Stack()

    visitedPositions.add(currentPos)

    var madeStep = false
    do
    {
        madeStep = false
        for (direction in listOf(Direction.North, Direction.South, Direction.West, Direction.East))
        {
            val nextPos = currentPos + direction.Vector2
            if (visitedPositions.contains(nextPos))
                continue // already visited
            val char = map[nextPos.y][nextPos.x]
            if (char != '.')
                continue // could not do step
            // here we could do a step, so update
            pathToStart.push(Direction.Opposite(direction))
            visitedPositions.add(nextPos)
            currentPos += direction.Vector2

            if (positionToNode.containsKey(currentPos))
            {
                val discoveredNode = positionToNode.getValue(currentPos)
                if (!node.HasTransitionToNode(discoveredNode))
                    node.CreateTransitionToNode(pathToStart.size, discoveredNode)
            }

            madeStep = true
            break
        }

        if (!madeStep)
        { // could not make a step on current position, backtrack
            if (pathToStart.isNotEmpty())
            {
                val direction = pathToStart.pop()
                currentPos += direction.Vector2
                madeStep = true
            }
        }
    } while (madeStep)
}

fun CreateAndConnectDeeperLevels(levels: Int)
{
    val newNodes: MutableSet<Node> = mutableSetOf()
    for (level in 0 until levels)
    {
        // copy nodes
        newNodes.clear()
        for (node in positionToNode.values)
        {
            val newNode = Node(node.position, node.name, level, node.innerPortal)
            newNodes.add(newNode)
        }
        // copy transitions
        for (node in positionToNode.values)
        {
            val newNode = newNodes.find { i -> i.name == node.name && i.innerPortal == node.innerPortal }
                ?: throw Exception("Should not occur")
            for (transition in node.transitions)
            {
                val transitionNode = transition.TakeTransitionFromNode(node)
                val transitionedNewNode = newNodes.find { i -> i.name == transitionNode.name && i.innerPortal == transitionNode.innerPortal }
                    ?: throw Exception("Should not occur")
                newNode.CreateTransitionToNode(transition.step, transitionedNewNode)
            }
        }
        allNodes.addAll(newNodes)
    }
    ConnectSamePortals(levels)
}

fun ConnectSamePortals(levels: Int)
{
    for (level in 0 until levels-1)
    {
        val currentLevelNodes = allNodes.filter { i -> i.level == level }
        val nextLevelNodes = allNodes.filter { i -> i.level == level+1 }
        for (currentLevelNode in currentLevelNodes)
        {
            if (currentLevelNode.innerPortal)
            {
                val nextLevelOuterPortalNode = nextLevelNodes.find { i -> i.name == currentLevelNode.name && !i.innerPortal } ?: throw Exception("Should not occur")
                currentLevelNode.CreateTransitionToNode(1, nextLevelOuterPortalNode)
            }
        }
    }
}

fun InvalidateUnavailablePortals()
{
    for (node in allNodes)
    {
        if (node.name == "AA" || node.name == "ZZ")
        {
            node.valid = node.level == 0
            continue
        }

        node.valid = node.innerPortal || node.level != 0
    }
}

fun GetShortestPath(fromNode : Node, targetNode : Node) : Int
{
    // Dijkstra's shortest path algorithm implemented from Wikipedia's pseudocode
    val Q : MutableSet<Node> = mutableSetOf()
    val distNodes : MutableMap<Node, Int> = mutableMapOf()
    val prevNodes : MutableMap<Node, Node?> = mutableMapOf()

    allNodes.forEach { i ->
        run {
            distNodes[i] = Int.MAX_VALUE
            prevNodes[i] = null
            Q.add(i)
        }
    }

    distNodes[fromNode] = 0

    while (Q.isNotEmpty())
    {
        val u = Q.minBy { i -> distNodes.getOrDefault(i, Int.MAX_VALUE) } ?: throw Exception("Cannot happen as Q not empty")
        Q.remove(u)

        if (!u.valid)
            continue

        val distanceU = distNodes.getValue(u)

        if (u == targetNode)
            return distanceU

        for (transition in u.transitions)
        {
            val v = transition.TakeTransitionFromNode(u)
            if (!Q.contains(v))
                continue
            val alt = distanceU + transition.step
            val distanceV =  distNodes.getValue(v)
            if (alt < distanceV)
            {
                distNodes[v] = alt
                prevNodes[v] = u
            }
        }
    }
    throw Exception("No path between $fromNode and $targetNode")
}

fun PrintScreenWithNodes()
{
    for (y in 0 until map.size)
    {
        for (x in 0 until mapWidth)
        {
            val node = positionToNode[IntVector2(x,y)]
            if (node != null)
            {
                if (node.innerPortal)
                    print(ConsoleColors.RED_BACKGROUND + map[y][x])
                else
                    print(ConsoleColors.GREEN_BACKGROUND + map[y][x])
            }
            else
                print(ConsoleColors.RESET + map[y][x])
        }
        println(ConsoleColors.RESET)
    }
}
