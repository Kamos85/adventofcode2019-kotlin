package aoc.december12.problem2

import IntVector3
import aoc.december12.moon.Moon
import java.io.File
import java.lang.Integer.max
import kotlin.math.min

val originalMoons : MutableList<Moon> = mutableListOf()
val moons : MutableList<Moon> = mutableListOf()

fun main()
{
    val input = File("input/December12-Problem.txt").readLines()
    val parseRegex = Regex("<x=(?<x>-?\\d+), y=(?<y>-?\\d+), z=(?<z>-?\\d+)>")
    for (i in input)
    {
        val groups = parseRegex.find(i)?.groups
        val xStr = groups?.get("x")?.value.orEmpty()
        val yStr = groups?.get("y")?.value.orEmpty()
        val zStr = groups?.get("z")?.value.orEmpty()
        moons.add(Moon(IntVector3(xStr.toInt(), yStr.toInt(), zStr.toInt())))
        originalMoons.add(Moon(IntVector3(xStr.toInt(), yStr.toInt(), zStr.toInt())))
    }

    var xLoop = 0
    var yLoop = 0
    var zLoop = 0

    var steps = 0
    do
    {
        UpdateMoonVelocities()
        UpdateMoonPositions()
        steps++

        if (xLoop == 0 && CheckXPositionsVelocitiesAsOriginal())
            xLoop = steps
        if (yLoop == 0 && CheckYPositionsVelocitiesAsOriginal())
            yLoop = steps
        if (zLoop == 0 && CheckZPositionsVelocitiesAsOriginal())
            zLoop = steps
    } while (xLoop == 0 || yLoop == 0 || zLoop == 0)

    val xyLoop = lcm(xLoop.toLong(), yLoop.toLong())
    val xyzLoop = lcm(xyLoop, zLoop.toLong())
    println("Answer: $xyzLoop")
}

fun gcd(a: Long, b: Long): Long = if (b == 0L) a else gcd(b, a % b)
fun lcm(a: Long, b: Long): Long = a / gcd(a, b) * b

fun UpdateMoonVelocities()
{
    for (i in 0 until moons.size)
        for (j in i+1 until moons.size)
            moons[i].ApplyGravityBetweenThisAndOtherMoon(moons[j])
}

fun UpdateMoonPositions()
{
    for (i in moons)
        i.UpdatePosition()
}

fun CheckXPositionsVelocitiesAsOriginal() : Boolean
{
    for (i in 0 until moons.size)
    {
       if (moons[i].position.x != originalMoons[i].position.x)
            return false;
        if (moons[i].velocity.x != originalMoons[i].velocity.x)
            return false;
    }
    return true
}

fun CheckYPositionsVelocitiesAsOriginal() : Boolean
{
    for (i in 0 until moons.size)
    {
        if (moons[i].position.y != originalMoons[i].position.y)
            return false;
        if (moons[i].velocity.y != originalMoons[i].velocity.y)
            return false;
    }
    return true
}

fun CheckZPositionsVelocitiesAsOriginal() : Boolean
{
    for (i in 0 until moons.size)
    {
        if (moons[i].position.z != originalMoons[i].position.z)
            return false;
        if (moons[i].velocity.z != originalMoons[i].velocity.z)
            return false;
    }
    return true
}
