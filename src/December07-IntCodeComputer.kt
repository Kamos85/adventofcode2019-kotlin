package aoc.december7.intcodecomputer

enum class Instruction(val value: Int)
{
    ADD(1),
    MULTIPLY(2),
    INPUT(3),
    OUTPUT(4),
    JUMP_IF_TRUE(5),
    JUMP_IF_FALSE(6),
    LESS_THAN(7),
    EQUALS(8),
    STOP(99)
}

class IntCodeComputer(var initialInstructionData: MutableList<Int>)
{
    var instructionData: MutableList<Int> = initialInstructionData.toMutableList()

    var instructionPointer = 0
    var inputStream = mutableListOf<Int>()
    var outputStream = mutableListOf<Int>()

    val parameterModes = mutableListOf(0,0,0)

    fun Run()
    {
        while (HandleInstruction())
            ;
    }

    fun IsComplete() : Boolean
    {
        val opcodeWithParameterModes = instructionData[instructionPointer]
        val opcode = opcodeWithParameterModes % 100
        return opcode == Instruction.STOP.value
    }

    fun Reset()
    {
        instructionData = initialInstructionData.toMutableList()

        instructionPointer = 0
        inputStream = mutableListOf<Int>()
        outputStream = mutableListOf<Int>()
    }

    fun AddInput(vararg input: Int)
    {
        inputStream.addAll(input.toList())
    }

    fun GetOutput() : Int
    {
        return outputStream.last()
    }

    fun HandleInstruction(): Boolean
    {
        val opcodeWithParameterModes = instructionData[instructionPointer]
        val opcode = opcodeWithParameterModes % 100
        val parameterModes = opcodeWithParameterModes / 100
        SetParameterModes(parameterModes)

        when (opcode)
        {
            Instruction.ADD.value -> Instruction_Add()
            Instruction.MULTIPLY.value -> Instruction_Multiply()
            Instruction.INPUT.value -> return Instruction_Input() // can halt when there is no input
            Instruction.OUTPUT.value -> Instruction_Output()
            Instruction.JUMP_IF_TRUE.value -> Instruction_Jump_If_True();
            Instruction.JUMP_IF_FALSE.value -> Instruction_Jump_If_False();
            Instruction.LESS_THAN.value -> Instruction_Less_Than();
            Instruction.EQUALS.value -> Instruction_Equals();
            Instruction.STOP.value -> return false;
            else -> println("Unknown instruction: $opcode")
        }
        return true
    }

    fun Instruction_Add()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        val value3 = instructionData[instructionPointer + 3]
        instructionData[value3] = value1 + value2
        instructionPointer += 4
    }

    fun GetValueFromParameter(parameterIndex: Int): Int
    {
        val parameterMode = parameterModes[parameterIndex - 1]
        val operandValue = instructionData[instructionPointer + parameterIndex]
        if (parameterMode == 0)
            return instructionData[operandValue]
        else
            return operandValue
    }

    fun Instruction_Multiply()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        val value3 = instructionData[instructionPointer + 3]
        instructionData[value3] = value1 * value2
        instructionPointer += 4
    }

    fun Instruction_Input() : Boolean
    {
        if (inputStream.isEmpty())
            return false; // halt
        val value1 = instructionData[instructionPointer + 1]
        instructionData[value1] = inputStream.removeAt(0)
        instructionPointer += 2
        return true;
    }

    fun Instruction_Output()
    {
        val value1 = GetValueFromParameter(1)
        outputStream.add(value1)
        instructionPointer += 2
    }

    fun Instruction_Jump_If_True()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        if (value1 != 0)
            instructionPointer = value2
        else
            instructionPointer += 3
    }

    fun Instruction_Jump_If_False()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        if (value1 == 0)
            instructionPointer = value2
        else
            instructionPointer += 3
    }

    fun Instruction_Less_Than()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        val value3 = instructionData[instructionPointer + 3]
        instructionData[value3] = if (value1 < value2) 1 else 0
        instructionPointer += 4
    }

    fun Instruction_Equals()
    {
        val value1 = GetValueFromParameter(1)
        val value2 = GetValueFromParameter(2)
        val value3 = instructionData[instructionPointer + 3]
        instructionData[value3] = if (value1 == value2) 1 else 0
        instructionPointer += 4
    }

    fun SetParameterModes(parameters: Int)
    {
        ResetParameterModes()
        var params = parameters
        var index = 0
        while (params > 0) {
            parameterModes[index] = params % 10
            params /= 10
            index++
        }
    }

    fun ResetParameterModes()
    {
        for (i in parameterModes.indices)
            parameterModes[i] = 0
    }
}
