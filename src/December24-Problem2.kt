// Approach to this problem:
// Use a dictionary (map) to store the different levels of maps
// Expand (add) to a new level only when necessary
// Change the calculation of neighbours to take into account the new connectivity
// Run for 200 steps and get the bug count across all maps

package aoc.december24.problem2

import IntVector2
import java.io.File
import java.lang.Exception

typealias Map = MutableList<MutableList<Char>>

val levelsToMaps : MutableMap<Int, Map> = mutableMapOf()
var mapHeight = -1
var mapWidth = -1

enum class Direction(val Vector2 : IntVector2)
{
    North(IntVector2(0, -1)),
    South(IntVector2(0, 1)),
    West(IntVector2(-1, 0)),
    East(IntVector2(1, 0));
}
val directions = listOf(Direction.North, Direction.South, Direction.West, Direction.East)

fun main()
{
    ParseMap()

    var steps = 0
    for (i in 1..200)
    {
        SimulateAllLevels()
        steps++
    }

    println("Answer: ${GetBugCount()}")
}

fun ParseMap()
{
    val map : Map = mutableListOf()
    val input = File("input/December24-Problem.txt").readLines()
    for (i in input)
        map.add(i.toCharArray().toMutableList())
    mapHeight = map.size
    mapWidth = map.last().size
    levelsToMaps[0] = map
}

fun SimulateAllLevels()
{
    DoLevelExpansion()

    val oldLevelsToMaps : MutableMap<Int, Map> = mutableMapOf()
    for (entry in levelsToMaps)
    {
        // copy map in entry
        val oldMap : Map = mutableListOf()
        for (oldRow in entry.value)
            oldMap.add(oldRow.toMutableList())
        oldLevelsToMaps[entry.key] = oldMap
    }

    for (entry in levelsToMaps)
        DoSimulationStep(oldLevelsToMaps, entry.key)
}

fun DoLevelExpansion()
{
    val minLevel = levelsToMaps.keys.min() ?: throw Exception("There are no values in levelsToMaps")
    val maxLevel = levelsToMaps.keys.max() ?: throw Exception("There are no values in levelsToMaps")

    val minLevelMap = levelsToMaps.getValue(minLevel)
    if (minLevelMap[1][2] == '#' ||
        minLevelMap[2][1] == '#' ||
        minLevelMap[3][2] == '#' ||
        minLevelMap[2][3] == '#')
    {
        val newEmptyMinLevelMap: Map = mutableListOf()
        for (y in 0 until mapHeight)
        {
            newEmptyMinLevelMap.add(mutableListOf())
            for (x in 0 until mapWidth)
                newEmptyMinLevelMap.last().add('.')
        }
        levelsToMaps[minLevel-1] = newEmptyMinLevelMap
    }

    val maxLevelMap = levelsToMaps.getValue(maxLevel)
    var shouldCreateNextLevel = false
    for (y in 0 until mapHeight)
        for (x in 0 until mapWidth)
            if (y in 1..mapHeight - 2 && x in 1..mapWidth - 2)
                continue
            else // on the outer bound
                if (maxLevelMap[y][x] == '#')
                    shouldCreateNextLevel = true

    if (shouldCreateNextLevel)
    {
        val newEmptyMaxLevelMap : Map = mutableListOf()
        for (y in 0 until mapHeight)
        {
            newEmptyMaxLevelMap.add(mutableListOf())
            for (x in 0 until mapWidth)
                newEmptyMaxLevelMap.last().add('.')
        }
        levelsToMaps[maxLevel+1] = newEmptyMaxLevelMap
    }
}

fun DoSimulationStep(oldLevelsToMaps : MutableMap<Int, Map>, currentLevel : Int)
{
    val map = levelsToMaps.getValue(currentLevel)
    val oldMap = oldLevelsToMaps.getValue(currentLevel)

    var bugNeighbours = 0
    for (y in 0 until mapHeight)
    {
        for (x in 0 until mapWidth)
        {
            if (y == 2 && x == 2)
                continue // center does nothing

            bugNeighbours = GetBugNeighboursInLevelForPosition(oldLevelsToMaps, currentLevel, x, y)

            if (oldMap[y][x] == '#')
            {
                if (bugNeighbours != 1)
                    map[y][x] = '.' // bug dies
            }
            else
            {
                if (bugNeighbours in 1..2)
                    map[y][x] = '#' // bug born
            }
        }
    }
}

fun GetBugNeighboursInLevelForPosition(oldLevelsToMaps : MutableMap<Int, Map>, currentLevel : Int, x : Int, y : Int) : Int
{
    val currentPosition = IntVector2(x, y)
    var bugNeighbours = 0
    for (direction in directions)
    {
        val neighbourPosition = currentPosition + direction.Vector2
        bugNeighbours += GetBugCountAt(oldLevelsToMaps, currentLevel, neighbourPosition.x, neighbourPosition.y, direction)
    }
    return bugNeighbours
}

fun GetBugCountAt(oldLevelsToMaps : MutableMap<Int, Map>, currentLevel : Int, x : Int, y : Int, toDirection : Direction) : Int
{
    val oldMap = oldLevelsToMaps.getValue(currentLevel)
    val innerMap = oldLevelsToMaps.getOrDefault(currentLevel+1, null)
    val outerMap = oldLevelsToMaps.getOrDefault(currentLevel-1, null)

    var bugCount = 0
    if (y == 2 && x == 2)
    {
        if (innerMap == null)
            return 0
        when (toDirection)
        {
            Direction.West -> {
                for (ny in 0 until mapHeight)
                    if (innerMap[ny][mapWidth-1] == '#')
                        bugCount++
            }
            Direction.East -> {
                for (ny in 0 until mapHeight)
                    if (innerMap[ny][0] == '#')
                        bugCount++
            }
            Direction.North -> {
                for (nx in 0 until mapWidth)
                    if (innerMap[mapHeight-1][nx] == '#')
                        bugCount++
            }
            Direction.South -> {
                for (nx in 0 until mapWidth)
                    if (innerMap[0][nx] == '#')
                        bugCount++
            }
        }
    } else
    {
        if (y in 0..mapHeight - 1 && x in 0..mapWidth - 1)
        { // not on border
            if (oldMap[y][x] == '#')
                bugCount++
        } else
        { // on outside border
            if (outerMap == null)
                return 0

            when (toDirection)
            {
                Direction.North -> {
                    if (outerMap[1][2] == '#')
                        bugCount++
                }
                Direction.South -> {
                    if (outerMap[3][2] == '#')
                        bugCount++
                }
                Direction.West -> {
                    if (outerMap[2][1] == '#')
                        bugCount++
                }
                Direction.East -> {
                    if (outerMap[2][3] == '#')
                        bugCount++
                }
            }
        }
    }
    return bugCount
}

fun GetBugCount() : Int
{
    var bugCount = 0
    for (entry in levelsToMaps)
        bugCount += entry.value.flatten().count { i -> i == '#' }
    return bugCount
}

fun PrintMap(map : Map)
{
    for (row in map)
        println(row.joinToString(""))
}
