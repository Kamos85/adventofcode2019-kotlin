package aoc.december14.problem2

import aoc.december14.reaction.Reaction
import java.io.File

val neededIngredients : MutableMap<String, Int> = mutableMapOf()
val reactions : MutableList<Reaction> = mutableListOf()
const val availableOre = 1000000000000L
var neededOre = 0L
var LeftOverIngredientsAfter1000Fuel : MutableMap<String, Int> = mutableMapOf()
var fuelProduced = 0
var requiredOreFor1000Fuel = 0L

fun main()
{
    Setup()
    neededIngredients["FUEL"] = 1000
    ProcessToGetNeededIngredients()
    LeftOverIngredientsAfter1000Fuel = neededIngredients.toMutableMap()
    for (i in LeftOverIngredientsAfter1000Fuel)
        i.setValue(-i.value)
    requiredOreFor1000Fuel = neededOre
    val milliFuels = (availableOre / requiredOreFor1000Fuel).toInt()
    fuelProduced = milliFuels * 1000
    neededOre = milliFuels * requiredOreFor1000Fuel
    for (i in neededIngredients)
        i.setValue(i.value*milliFuels)
    println(neededIngredients)

    var prevOreRequired = neededOre
    do
    {
        prevOreRequired = neededOre
        Revert1000Fuel()
        do
        {
            neededIngredients["FUEL"] = 1
            fuelProduced++
            ProcessToGetNeededIngredients()
        } while (neededOre < availableOre)
    } while (neededOre != prevOreRequired)

    fuelProduced-- // we counted one too many fuel (because the inner loop goes over the available ore)
    println("Answer: $fuelProduced")
}

fun Revert1000Fuel()
{
    fuelProduced -= 1000
    neededOre -= requiredOreFor1000Fuel
    for (i in LeftOverIngredientsAfter1000Fuel)
        neededIngredients.set(i.key, neededIngredients.getOrDefault(i.key, 0) + i.value)
}

fun Setup()
{
    val input = File("input/December14-Problem.txt").readLines()
    val parseReactionRegex = Regex("(?<Inputs>.+) => (?<OutputAmount>\\d+) (?<OutputProduct>.+)")
    val parseInputTermRegex = Regex("(?<Amount>\\d+) (?<Product>.+)")

    for (i in input)
    {
        val reactionGroups = parseReactionRegex.find(i)?.groups
        val inputs = reactionGroups?.get("Inputs")?.value.orEmpty()
        val outputAmount = reactionGroups?.get("OutputAmount")?.value.orEmpty()
        val outputProduct = reactionGroups?.get("OutputProduct")?.value.orEmpty()
        val inputTerms = inputs.split(",")
        val reaction = Reaction(outputProduct, outputAmount.toInt())
        for (j in inputTerms)
        {
            val inputTermGroups = parseInputTermRegex.find(j)?.groups
            val inputAmount = inputTermGroups?.get("Amount")?.value.orEmpty()
            val inputProduct = inputTermGroups?.get("Product")?.value.orEmpty()
            reaction.AddInput(inputProduct, inputAmount.toInt())
        }
        reactions.add(reaction)
    }
}

fun ProcessToGetNeededIngredients()
{
    do
    {
        val ingr = neededIngredients.entries.find { i -> i.value > 0 }
        if (ingr == null)
            break
        RevertReaction(ingr.key, ingr.value)
    } while (neededIngredients.any { e -> e.value > 0})
}

fun RevertReaction(outputProduct : String, outputAmount : Int)
{
    val r = reactions.find { i -> i.outputProduct == outputProduct }
    if (r == null)
        throw Exception("There is no reaction for $outputProduct")

    neededIngredients[outputProduct] = neededIngredients.getOrDefault(outputProduct, 0) - r.outputAmount

    r.inputs.forEach { i -> AddToNeededIngredients(i.key, i.value) }
}

fun AddToNeededIngredients(product : String, amount : Int)
{
    if (product == "ORE")
    {
        neededOre += amount
        return
    }

    neededIngredients[product] = amount + neededIngredients.getOrDefault(product, 0)
}
