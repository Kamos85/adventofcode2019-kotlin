class IntVector3(var x : Int, var y : Int, var z : Int)
{
    override fun equals(other: Any?): Boolean
    {
        if (other == null || other !is IntVector3) return false
        return x == other.x && y == other.y && z == other.z
    }

    override fun hashCode(): Int
    {
        var hashCode = 1861411795;
        hashCode = hashCode * -1521134295 + x.hashCode();
        hashCode = hashCode * -1521134295 + y.hashCode();
        hashCode = hashCode * -1521134295 + z.hashCode();
        return hashCode;
    }

    override fun toString(): String
    {
        return "($x,$y,$z)";
    }

    operator fun plus(other: IntVector3): IntVector3
    {
        return IntVector3(x + other.x, y + other.y, z + other.z)
    }

    val LengthSqr: Int
        get() = x*x + y*y + z*z
}
