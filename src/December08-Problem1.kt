package aoc.december8.problem1

import java.io.File

val imageWidth = 25
val imageHeight = 6
val layers : MutableList<MutableList<MutableList<Int>>> = mutableListOf()

fun main()
{
    Setup()

    val fewest0LayerIndex = FindLayerWithFewest0Digits()
    val digit1Count = layers[fewest0LayerIndex].flatten().count{i -> i == 1}
    val digit2Count = layers[fewest0LayerIndex].flatten().count{i -> i == 2}

    val answer = digit1Count * digit2Count
    println("Answer: $answer")
}

fun Setup()
{
    val input: String = File("input/December08-Problem.txt").readText()
    var layerIndex = 1

    var parseIndex = 0
    while (parseIndex < input.length)
    {
        layers.add(mutableListOf(mutableListOf()))
        for (y in 0 until imageHeight)
        {
            if (y > 0) // dont add row for the first row, it's already there
                layers.last().add(mutableListOf())
            for (x in 0 until imageWidth)
            {
                layers.last().last().add(input[parseIndex].toString().toInt())
                parseIndex++
            }
        }
    }
}

fun FindLayerWithFewest0Digits() : Int
{
    var layerIndex = -1
    var lowest0Count = Int.MAX_VALUE;

    for ((index, layer) in layers.withIndex())
    {
        val count0 = layer.flatten().count{i -> i == 0}
        if (count0 < lowest0Count)
        {
            lowest0Count = count0
            layerIndex = index
        }
    }

    return layerIndex
}
