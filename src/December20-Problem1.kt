// Approach to this problem:
// Discover all portals with their adjacent open spot as node
// Connect portal nodes with the same name by a transition of 1 step
// For each portal create transitions to other portals it is connected to
// Run Dijkstra's shortest path algorithm

package aoc.december20.problem1

import IntVector2
import aoc.december20.nodeandtransition.Node
import java.io.File
import java.util.*

enum class Direction(val value: Int, val Vector2 : IntVector2)
{
    North(1, IntVector2(0,-1)),
    South(2, IntVector2(0,1)),
    West(3, IntVector2(-1,0)),
    East(4, IntVector2(1,0));

    companion object
    {
        fun Opposite(direction : Direction) : Direction
        {
            return when (direction)
            {
                North -> South
                South -> North
                West -> East
                East -> West
            }
        }
    }
}

val map : MutableList<MutableList<Char>> = mutableListOf()
var mapWidth = 0
val positionToNode : MutableMap<IntVector2, Node> = mutableMapOf()

fun main()
{
    ParseMap()
    DiscoverPortals()
    ConnectSamePortals()
    DiscoverAndConnectPortals()
    val startNode = positionToNode.values.find { i -> i.name == "AA" } ?: throw Exception("There is no start node AA")
    val endNode = positionToNode.values.find { i -> i.name == "ZZ" } ?: throw Exception("There is no start node ZZ")
    val answer = GetShortestPath(startNode, endNode)
    PrintScreenWithNodes()
    println("Answer: $answer")
}

fun ParseMap()
{
    val input = File("input/December20-Problem.txt").readLines()
    for (line in input)
    {
        map.add(mutableListOf())
        for (c in line)
            map.last().add(c)
    }
    mapWidth = map.last().size
}

fun DiscoverPortals()
{
    val readPortalPositions : MutableSet<IntVector2> = mutableSetOf()

    for (y in 0 until map.size)
    {
        for (x in 0 until mapWidth)
        {
            val char = map[y][x]
            if (char == ' ' || char == '#' || char == '.')
                continue
            val position = IntVector2(x,y)
            if (readPortalPositions.contains(position))
                continue
            ParseNode(position, readPortalPositions)
        }
    }
}

fun ParseNode(position : IntVector2, readPortalPositions : MutableSet<IntVector2>)
{
    var portalName = map[position.y][position.x].toString()
    var portalPosition = IntVector2(-1,-1)
    readPortalPositions.add(position)
    val horizontalLetter = map[position.y][position.x+1]
    if (horizontalLetter != ' ' && horizontalLetter != '#' && horizontalLetter != '.')
    { // horizontal portal name
        portalName += horizontalLetter
        readPortalPositions.add(position + IntVector2(1,0))

        val left = map[position.y].getOrElse(position.x-1) {' '}
        if (left == '.')
            portalPosition = position + IntVector2(-1,0)
        else
            portalPosition = position + IntVector2(2,0)
    } else
    {
        portalName += map[position.y + 1][position.x]
        readPortalPositions.add(position + IntVector2(0,1))

        val top = map.getOrElse(position.y-1) { mutableListOf() }
        if (top.size > 0 && top[position.x] == '.')
            portalPosition = position + IntVector2(0,-1)
        else
            portalPosition = position + IntVector2(0,2)
    }

    positionToNode[portalPosition] = Node(portalPosition, portalName)
}

fun ConnectSamePortals()
{
    val nodes = positionToNode.values.toList()
    for (i in 0 until nodes.size)
        for (j in i+1 until nodes.size)
            if (nodes[i].name == nodes[j].name)
                nodes[i].CreateTransitionToNode(1, nodes[j])
}

fun DiscoverAndConnectPortals()
{
    for (i in positionToNode)
        DiscoverAndConnectPortal(i.value)
}

fun DiscoverAndConnectPortal(node : Node)
{
    val visitedPositions : MutableSet<IntVector2> = mutableSetOf()
    var currentPos = node.position
    val pathToStart : Stack<Direction> = Stack()

    visitedPositions.add(currentPos)

    var madeStep = false
    do
    {
        madeStep = false
        for (direction in listOf(Direction.North, Direction.South, Direction.West, Direction.East))
        {
            val nextPos = currentPos + direction.Vector2
            if (visitedPositions.contains(nextPos))
                continue // already visited
            val char = map[nextPos.y][nextPos.x]
            if (char != '.')
                continue // could not do step
            // here we could do a step, so update
            pathToStart.push(Direction.Opposite(direction))
            visitedPositions.add(nextPos)
            currentPos += direction.Vector2

            if (positionToNode.containsKey(currentPos))
            {
                val discoveredNode = positionToNode.getValue(currentPos)
                if (!node.HasTransitionToNode(discoveredNode))
                    node.CreateTransitionToNode(pathToStart.size, discoveredNode)
            }

            madeStep = true
            break
        }

        if (!madeStep)
        { // could not make a step on current position, backtrack
            if (pathToStart.isNotEmpty())
            {
                val direction = pathToStart.pop()
                currentPos += direction.Vector2
                madeStep = true
            }
        }
    } while (madeStep)
}

fun GetShortestPath(fromNode : Node, targetNode : Node) : Int
{
    // Dijkstra's shortest path algorithm implemented from Wikipedia's pseudocode
    val Q : MutableSet<Node> = mutableSetOf()
    val distNodes : MutableMap<Node, Int> = mutableMapOf()
    val prevNodes : MutableMap<Node, Node?> = mutableMapOf()

    positionToNode.forEach { i ->
        run {
            distNodes[i.value] = Int.MAX_VALUE
            prevNodes[i.value] = null
            Q.add(i.value)
        }
    }

    distNodes[fromNode] = 0

    while (Q.isNotEmpty())
    {
        val u = Q.minBy { i -> distNodes.getOrDefault(i, Int.MAX_VALUE) } ?: throw Exception("Cannot happen as Q not empty")
        Q.remove(u)

        val distanceU = distNodes.getValue(u)

        if (u == targetNode)
            return distanceU

        for (transition in u.transitions)
        {
            val v = transition.TakeTransitionFromNode(u)
            if (!Q.contains(v))
                continue
            val alt = distanceU + transition.step
            val distanceV =  distNodes.getValue(v)
            if (alt < distanceV)
            {
                distNodes[v] = alt
                prevNodes[v] = u
            }
        }
    }
    throw Exception("No path between $fromNode and $targetNode")
}

fun PrintScreenWithNodes()
{
    for (y in 0 until map.size)
    {
        for (x in 0 until mapWidth)
        {
            if (positionToNode.containsKey(IntVector2(x,y)))
                print(ConsoleColors.RED_BACKGROUND + map[y][x])
            else
                print(ConsoleColors.RESET + map[y][x])
        }
        println(ConsoleColors.RESET)
    }
}
