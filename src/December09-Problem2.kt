package aoc.december9.problem2

import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File

fun main()
{
    val input: String = File("input/December09-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    val program = IntCodeComputer(instructionData)

    program.AddInput(2)
    program.Run()

    println("Answer:")
    while (program.HasOutput())
        println(program.GetOutput())
}
