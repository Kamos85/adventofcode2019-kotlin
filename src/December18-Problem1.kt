package aoc.december18.problem1

import IntVector2
import aoc.december15.problem2.directions
import java.io.File
import java.lang.Integer.min

val map : MutableList<MutableList<Char>> = mutableListOf()
var mapWidth = -1;
var startNode : Node = Node(IntVector2(-1,-1))
val positionToNode = mutableMapOf<IntVector2, Node>()
val keyToNode = mutableMapOf<String, Node>()
var totalKeysToCollect = 0
var cachedPathDistances : MutableMap<Node, MutableMap<Node, Int>> = mutableMapOf()

enum class Direction(val value: Int, val Vector2 : IntVector2)
{
    North(1, IntVector2(0, -1)),
    South(2, IntVector2(0, 1)),
    West(3, IntVector2(-1, 0)),
    East(4, IntVector2(1, 0));
}

class Transition(var node1 : Node, var step : Int, var node2 : Node)
{
    fun TakeTransitionFromNode(node : Node) : Node
    {
        if (node == node1)
            return node2
        if (node == node2)
            return node1
        throw Exception("This transition cannot be taken from the given node")
    }

    fun Remove()
    {
        node1.RemoveTransition(this)
        node2.RemoveTransition(this)
    }

    override fun toString(): String
    {
        return "$node1 --$step-- $node2"
    }
}

class Node(var position : IntVector2)
{
    var key = ""
    var door = ""

    val transitions = mutableListOf<Transition>()

    fun GetTransitionCount() : Int
    {
        return transitions.size
    }

    fun GetOnlySingleTransition() : Transition
    {
        if (transitions.size != 1)
            throw Exception("This can only be called when there is a single transition")
        return transitions[0]
    }

    fun CreateTransitionToNode(step : Int, node : Node)
    {
        val transition = Transition(this, step, node)
        transitions.add(transition)
        node.AddTransitionFromOtherNode(transition)
    }

    private fun AddTransitionFromOtherNode(transition: Transition)
    {
        transitions.add(transition)
    }

    fun RemoveTransition(transition : Transition)
    {
        transitions.remove(transition)
    }

    override fun toString(): String
    {
        var result = position.toString()
        if (key.isNotEmpty())
            result += "($key)"
        if (door.isNotEmpty())
            result += "($door)"
        return result
    }
}

val cache : MutableMap<MutableSet<String>, Int> = mutableMapOf()

fun main()
{
    ParseMap()
    CreateGraph()
    FixConnectionIssuesForDay18Input()

    positionToNode.forEach { i ->
        run {
            if (i.value.key.isNotEmpty())
            {
                totalKeysToCollect++
                keyToNode[i.value.key] = i.value
            }
        }
    }
    PrintScreenWithNodes()
    println("totalKeysToCollect: $totalKeysToCollect")
    val minSteps = GetNextKey(startNode, mutableSetOf())
    println("Answer: $minSteps")
}

fun ParseMap()
{
    val input = File("input/December18-Day-1-Problem.txt").readLines()
    for (i in input)
        map.add(i.toCharArray().toMutableList())
    mapWidth = map.last().size
}

val visitedPositions = mutableSetOf<IntVector2>()
fun CreateGraph()
{
    val startPosition = FindStartPosition()
    processMapEntry(null, startPosition, 0)
    startNode = positionToNode[startPosition]?:throw Exception("No start node")
}

fun processMapEntry(fromNode : Node?, position : IntVector2, steps : Int)
{
    visitedPositions.add(position)

    val nextOpenPositions = mutableListOf<IntVector2>()
    for (direction in directions)
    {
        val nextPos = position + direction.Vector2
        val nextPosChar = map[nextPos.y][nextPos.x]
        if (nextPosChar == '#')
            continue
        if (visitedPositions.contains(nextPos))
            continue
        nextOpenPositions.add(nextPos)
    }

    val posChar = map[position.y][position.x]
    if (posChar != '.' || nextOpenPositions.size >= 2)
    { // create a node
        val newNode = Node(position)
        if (posChar != '.' && posChar != '@')
        {
            val asciiChar = map[position.y][position.x].toChar()
            if (asciiChar.toInt() in 65..90)
                newNode.door = asciiChar.toString()
            if (asciiChar.toInt() in 97..122)
                newNode.key = asciiChar.toString()
        }
        for (next in nextOpenPositions)
            processMapEntry(newNode, next, 1)
        if (posChar == '.' && newNode.GetTransitionCount() == 0)
            return  // this node has no use (leads to empty paths)
        if (posChar == '.' && newNode.GetTransitionCount() == 1)
        { // this node only links to the next one and should not be a node
            val transition = newNode.GetOnlySingleTransition()
            fromNode?.CreateTransitionToNode(steps + transition.step, transition.TakeTransitionFromNode(newNode))
            return
        }
        positionToNode[position] = newNode
        fromNode?.CreateTransitionToNode(steps, newNode)
    } else
    {
        if (nextOpenPositions.size == 1)
            processMapEntry(fromNode, nextOpenPositions[0], steps + 1)
    }
}

fun FindStartPosition() : IntVector2
{
    for (y in 0 until map.size)
        for (x in 0 until mapWidth)
            if (map[y][x] == '@')
                return IntVector2(x,y)
    throw Exception("Start node not found")
}

fun FixConnectionIssuesForDay18Input()
{
    if (startNode.position != IntVector2(40,40))
        return
    println("fix starting position")
    // first remove wrong node connections and create new nodes if not existent
    for (y in 39..41)
    {
        for (x in 39..41)
        {
            val node = positionToNode[IntVector2(x,y)]
            if (node == null)
            {
                positionToNode[IntVector2(x,y)] = Node(IntVector2(x,y))
                continue
            }
            for (i in node.transitions.size-1 downTo 0)
            {
                val otherNode = node.transitions[i].TakeTransitionFromNode(node)
                if (otherNode.position.x in 39..41 && otherNode.position.y in 39..41)
                    node.transitions[i].Remove()
            }
        }
    }
    // now rebuild connections
    for (y in 39..41)
    {
        for (x in 39..41)
        {
            val nodePos = IntVector2(x, y)
            val node = positionToNode[nodePos] ?: throw Exception("Cannot be, node must exist here")
            for (direction in listOf(Direction.East, Direction.South))
            {
                val nextPos = nodePos + direction.Vector2
                if (nextPos.x in 39..41 && nextPos.y in 39..41)
                {
                    val otherNode = positionToNode[nextPos] ?: throw Exception("Cannot be, node must exist here")
                    node.CreateTransitionToNode(1, otherNode)
                }
            }
        }
    }
}

fun GetNextKey(currentNode : Node, collectedKeys : MutableSet<String>) : Int
{
    val accessibleUncollectedKeys = GetDirectUncollectedAccessibleKeys(collectedKeys)
    if (accessibleUncollectedKeys.size == 0)
        return 0

    val cacheKey = accessibleUncollectedKeys.toMutableSet()
    cacheKey.add("$" + currentNode.key)
    if (currentNode.key.isNotEmpty())
    {
        val cacheResult = cache[cacheKey]
        if (cacheResult != null)
            return cacheResult
    }

    var minSteps = Int.MAX_VALUE
    for (key in accessibleUncollectedKeys)
    {
        val targetNode = keyToNode[key]
        if (targetNode == null)
            throw Exception("Key $key has no node")

        val distanceToKey = GetStepsToNode(currentNode, targetNode)
        collectedKeys.add(key)
        val totalSteps = distanceToKey + GetNextKey(targetNode, collectedKeys)
        minSteps = min(minSteps, totalSteps)
        collectedKeys.remove(key)
    }

    if (currentNode.key.isNotEmpty())
        cache[cacheKey] = minSteps

    return minSteps
}

val visitedNodesForKeys = mutableSetOf<Node>()
fun GetDirectUncollectedAccessibleKeys(collectedKeys : MutableSet<String>) : MutableSet<String>
{
    val accessibleKeys = mutableSetOf<String>()

    visitedNodesForKeys.clear()
    VisitNodeForKeys(startNode, collectedKeys, accessibleKeys)
    return accessibleKeys
}

fun VisitNodeForKeys(node : Node, collectedKeys: MutableSet<String>, accessibleKeys : MutableSet<String>)
{
    visitedNodesForKeys.add(node)

    if (node.key.isNotEmpty())
    {
        if (!collectedKeys.contains(node.key))
        {
            accessibleKeys.add(node.key)
            return
        }
    }

    if (node.door.isNotEmpty())
    {
        val key = node.door.toLowerCase()
        if (!collectedKeys.contains(key))
            return
    }

    for (transition in node.transitions)
    {
        val otherNode = transition.TakeTransitionFromNode(node)
        if (!visitedNodesForKeys.contains(otherNode))
            VisitNodeForKeys(otherNode, collectedKeys, accessibleKeys)
    }
}

fun GetStepsToNode(fromNode : Node, targetNode: Node) : Int
{
    if (cachedPathDistances.containsKey(fromNode))
    {
        val toNodes = cachedPathDistances[fromNode] ?: throw Exception("Can't happen")
        val result = toNodes[targetNode]
        if (result != null)
            return result
    }

    // Dijkstra's shortest path algorithm implemented from Wikipedia's pseudocode
    val Q : MutableSet<Node> = mutableSetOf()
    val distNodes : MutableMap<Node, Int> = mutableMapOf()
    val prevNodes : MutableMap<Node, Node?> = mutableMapOf()

    positionToNode.forEach { i ->
        run {
            distNodes[i.value] = Int.MAX_VALUE
            prevNodes[i.value] = null
            Q.add(i.value)
        }
    }

    distNodes[fromNode] = 0

    while (Q.isNotEmpty())
    {
        val u = Q.minBy { i -> distNodes.getOrDefault(i, Int.MAX_VALUE) } ?: throw Exception("Cannot happen as Q not empty")
        Q.remove(u)

        val distanceU = distNodes[u] ?: throw Exception("target node has no distance set")

        if (u == targetNode)
        {
            if (!cachedPathDistances.containsKey(fromNode))
                cachedPathDistances[fromNode] = mutableMapOf()
            val toNodes = cachedPathDistances[fromNode] ?: throw Exception("Can't happen")
            toNodes[targetNode] = distanceU
            return distanceU
        }

        for (transition in u.transitions)
        {
            val v = transition.TakeTransitionFromNode(u)
            if (!Q.contains(v))
                continue
            val alt = distanceU + transition.step
            val distanceV =  distNodes[v] ?: throw Exception("node v no distance set")
            if (alt < distanceV)
            {
                distNodes[v] = alt
                prevNodes[v] = u
            }
        }
    }
    throw Exception("No path between $fromNode and $targetNode")
}

fun PrintScreenWithNodes()
{
    for (y in 0 until map.size)
    {
        for (x in 0 until mapWidth)
        {
            if (positionToNode.containsKey(IntVector2(x,y)))
                print(ConsoleColors.RED_BACKGROUND + map[y][x])
            else
                print(ConsoleColors.RESET + map[y][x])
        }
        println(ConsoleColors.RESET)
    }
}
