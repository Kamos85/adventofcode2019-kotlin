class DoubleVector2(var x : Double, var y : Double)
{
    override fun equals(other: Any?): Boolean
    {
        if (other == null || other !is DoubleVector2) return false
        return x == other.x && y == other.y
    }

    override fun hashCode(): Int
    {
        var hashCode = 1861411795;
        hashCode = hashCode * -1521134295 + x.hashCode();
        hashCode = hashCode * -1521134295 + y.hashCode();
        return hashCode;
    }

    override fun toString(): String
    {
        return "($x,$y)";
    }

    operator fun plus(other: DoubleVector2): DoubleVector2
    {
        return DoubleVector2(x + other.x, y + other.y)
    }

    operator fun minus(other: DoubleVector2): DoubleVector2
    {
        return DoubleVector2(x - other.x, y - other.y)
    }

    val LengthSqr: Double
        get() = x*x + y*y
}
