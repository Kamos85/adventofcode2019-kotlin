package aoc.december18.nodeandtransition

import IntVector2

class Node(var position : IntVector2)
{
    var key = ""
    var door = ""

    val transitions = mutableListOf<Transition>()

    fun GetTransitionCount() : Int
    {
        return transitions.size
    }

    fun GetOnlySingleTransition() : Transition
    {
        if (transitions.size != 1)
            throw Exception("This can only be called when there is a single transition")
        return transitions[0]
    }

    fun CreateTransitionToNode(step : Int, node : Node)
    {
        val transition = Transition(this, step, node)
        transitions.add(transition)
        node.AddTransitionFromOtherNode(transition)
    }

    private fun AddTransitionFromOtherNode(transition: Transition)
    {
        transitions.add(transition)
    }

    override fun toString(): String
    {
        var result = position.toString()
        if (key.isNotEmpty())
            result += "($key)"
        if (door.isNotEmpty())
            result += "($door)"
        return result
    }
}

class Transition(var node1: Node, var step: Int, var node2: Node)
{
    fun TakeTransitionFromNode(node : Node): Node
    {
        if (node == node1)
            return node2
        if (node == node2)
            return node1
        throw Exception("This transition cannot be taken from the given node")
    }

    override fun toString(): String
    {
        return "$node1 --$step-- $node2"
    }
}
