// Approach to this problem:
// Go over possible noun and verb values until the required output is obtained

package aoc.december2.problem2

import java.io.File

val desiredOutput = 19690720

var instructionData : MutableList<Int> = mutableListOf()
var originalInstructionData : List<Int> = listOf()

enum class Instruction(val value: Int) {
    ADD(1),
    MULTIPLY(2),
    STOP(99)
}

fun main()
{
    val input: String = File("input/December02-Problem.txt").readText()
    originalInstructionData = input.split(",").map { it.toInt() }

    val answer = findSolution()

    println("Answer: $answer")
}

fun findSolution() : Int
{
    var output : Int;
    for (noun in 0 until 100)
    {
        for (verb in 0 until 100)
        {
            output = getSimulationAnswer(noun, verb)
            if (output == desiredOutput)
                return 100 * noun + verb
        }
    }
    return -1
}

fun getSimulationAnswer(noun : Int, verb : Int) : Int
{
    // reset simulation data
    instructionData = originalInstructionData.toMutableList()

    // apply instruction as given in problem
    instructionData[1] = noun
    instructionData[2] = verb

    for (x in 0 until instructionData.size step 4)
    {
        if (instructionData[x] == Instruction.STOP.value)
            break;
        handleInstruction(instructionData[x], instructionData[x+1], instructionData[x+2], instructionData[x+3])
    }

    return instructionData[0]
}

fun handleInstruction(instructionType : Int, inputOperand1 : Int, inputOperand2 : Int, outputOperand : Int)
{
    when (instructionType)
    {
        Instruction.ADD.value -> instructionData[outputOperand] = instructionData[inputOperand1] + instructionData[inputOperand2]
        Instruction.MULTIPLY.value -> instructionData[outputOperand] = instructionData[inputOperand1] * instructionData[inputOperand2]
        else -> println("Unknown instruction: $instructionType")
    }
}
