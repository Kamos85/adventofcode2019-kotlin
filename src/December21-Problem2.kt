// Approach to this problem:
// Like part 1. Generation of code or adjusting of code is hard to do, checking by hand was easiest

package aoc.december21.problem2

import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File

fun main()
{
    val input: String = File("input/December21-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    val program = IntCodeComputer(instructionData)

    SendSprintScriptToProgram(program, listOf(
        "OR A T",
        "AND B T",
        "AND C T",
        "NOT T T",
        "OR E J",
        "OR H J",
        "AND D J",
        "AND T J"
    ))

    program.Run()
    var programOutput = ""
    while (program.HasOutput())
    {
        val output = program.GetOutput()
        if (output > 255)
            programOutput += output
        else
            programOutput += output.toChar()
    }
    println(programOutput)
    val lastAtIndex = programOutput.indexOfLast { i -> i == '@' }
    if (lastAtIndex < 0)
    {
        println("There was no @ found")
    }
    else
    {
        println(programOutput.substring(lastAtIndex - 3, lastAtIndex + 5))
        println("ABCDEFGHI")
    }
}

fun SendSprintScriptToProgram(program : IntCodeComputer, springScriptLines : List<String>)
{
    for (line in springScriptLines)
    {
        for (c in line)
            program.AddInput(c.toLong())
        program.AddInput(10)
    }
    for (c in "RUN")
        program.AddInput(c.toLong())
    program.AddInput(10)
}
