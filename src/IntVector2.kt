class IntVector2(var x : Int, var y : Int)
{
    override fun equals(other: Any?): Boolean
    {
        if (other == null || other !is IntVector2) return false
        return x == other.x && y == other.y
    }

    override fun hashCode(): Int
    {
        var hashCode = 1861411795;
        hashCode = hashCode * -1521134295 + x.hashCode();
        hashCode = hashCode * -1521134295 + y.hashCode();
        return hashCode;
    }

    override fun toString(): String
    {
        return "($x,$y)";
    }

    operator fun plus(other: IntVector2): IntVector2
    {
        return IntVector2(x + other.x, y + other.y)
    }

    operator fun minus(other: IntVector2): IntVector2
    {
        return IntVector2(x - other.x, y - other.y)
    }

    val LengthSqr: Int
        get() = x*x + y*y
}
