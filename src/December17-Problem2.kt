package aoc.december17.problem2

import IntVector2
import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File
import java.lang.Exception

enum class Direction(val value: Int, val Vector2 : IntVector2)
{
    North(1, IntVector2(0, -1)),
    South(2, IntVector2(0, 1)),
    West(3, IntVector2(-1, 0)),
    East(4, IntVector2(1, 0));

    companion object
    {
        fun GetTurn(from : Direction, to : Direction) : String
        {
            return when (from)
            {
                North -> when (to)
                {
                    North -> ""
                    South -> ""
                    West -> "L"
                    East -> "R"
                }
                South -> when (to)
                {
                    North -> ""
                    South -> ""
                    West -> "R"
                    East -> "L"
                }
                West -> when (to)
                {
                    North -> "R"
                    South -> "L"
                    West -> ""
                    East -> ""
                }
                East -> when (to)
                {
                    North -> "L"
                    South -> "R"
                    West -> ""
                    East -> ""
                }
            }
        }
    }
}

val map : MutableList<MutableList<Char>> = mutableListOf()
var mapWidth = 0
val intersections : MutableSet<IntVector2> = mutableSetOf()

fun main()
{
    val input: String = File("input/December17-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    val program = IntCodeComputer(instructionData)

    map.add(mutableListOf())
    program.Run()
    while (program.HasOutput())
    {
        val outputChar = program.GetOutput()
        if (outputChar == 10L)
            map.add(mutableListOf())
        else
            map.last().add(outputChar.toChar())
    }
    // remove last two entries, they are empty
    map.removeAt(map.size - 1)
    map.removeAt(map.size - 1)
    mapWidth = map.last().size

    PrintScreen()

    // Build path
    var path = ""
    var robotDirection = Direction.North
    var newRobotDirection = Direction.North
    var robotPosition = FindRobot()
    var forward = 0

    while (0 <= robotPosition.x && robotPosition.x < mapWidth && 0 <= robotPosition.y && robotPosition.y < map.size)
    {
        newRobotDirection = GetPathDirection(robotPosition, robotDirection)
        val turn = Direction.GetTurn(robotDirection, newRobotDirection)
        if (turn.isNotEmpty())
        {
            if (forward > 0)
            {
                path += forward
                forward = 0
            }
            path += turn
            robotDirection = newRobotDirection
        }
        forward++
        robotPosition += robotDirection.Vector2
    }
    forward--  // went one too far
    if (forward > 0)
        path += forward

    println(path)
    // R12L10R12 L8R10R6 R12L10R12 R12L10R10L8 L8R10R6 R12L10R10L8 L8R10R6 R12L10R10L8 R12L10R12 R12L10R10L8
    // A = R12L10R12
    // B = L8R10R6
    // C = R12L10R10L8
    // A,B,A,C,B,C,B,C,A,C

    program.Reset()
    program.StoreInMemory(0, 2)

    val main = "A,B,A,C,B,C,B,C,A,C"
    for (c in main)
        program.AddInput(c.toLong())
    program.AddInput(10L)

    val A = "R,12,L,10,R,12"
    for (c in A)
        program.AddInput(c.toLong())
    program.AddInput(10L)

    val B = "L,8,R,10,R,6"
    for (c in B)
        program.AddInput(c.toLong())
    program.AddInput(10L)

    val C = "R,12,L,10,R,10,L,8"
    for (c in C)
        program.AddInput(c.toLong())
    program.AddInput(10L)

    program.AddInput('n'.toLong())
    program.AddInput(10L)

    program.Run()

    var lastValue = 0L
    while (program.HasOutput())
        lastValue = program.GetOutput()

    println("Answer: $lastValue")
}

fun PrintScreen()
{
    map.forEach { i -> println(i.joinToString("")) }
}

fun FindRobot() : IntVector2
{
    for (y in 0 until map.size)
        for (x in 0 until mapWidth)
            if (map[y][x] == '^')
                return IntVector2(x, y)
    throw Exception("Could not find robot")
}

fun GetPathDirection(position : IntVector2, direction : Direction) : Direction
{
    val canGoNorth = position.y > 0 && map[position.y-1][position.x] == '#'
    val canGoSouth = position.y < map.size-1 && map[position.y+1][position.x] == '#'
    val canGoWest = position.x > 0 && map[position.y][position.x-1] == '#'
    val canGoEast = position.x < mapWidth-1 && map[position.y][position.x+1] == '#'

    when (direction)
    {
        Direction.North -> {
            if (canGoNorth) return Direction.North
            if (canGoWest) return Direction.West
            if (canGoEast) return Direction.East
        }
        Direction.South -> {
            if (canGoSouth) return Direction.South
            if (canGoWest) return Direction.West
            if (canGoEast) return Direction.East
        }
        Direction.West -> {
            if (canGoWest) return Direction.West
            if (canGoNorth) return Direction.North
            if (canGoSouth) return Direction.South
        }
        Direction.East -> {
            if (canGoEast) return Direction.East
            if (canGoNorth) return Direction.North
            if (canGoSouth) return Direction.South
        }
    }
    // if execution reaches here robot is near end of path
    return direction
}
