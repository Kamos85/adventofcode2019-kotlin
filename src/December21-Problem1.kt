// Approach to this problem:
// I was tempted to write a proper solution to this problem where it would either generate a lot of possible code
// and run the program to see when it would succeed but there are a lot of possible combinations or look at the output
// and programmatically adjust the program every time it failed.
// As the input was limited I first tried it a bit by hand and already found the solution

package aoc.december21.problem1

import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File

fun main()
{
    val input: String = File("input/December21-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    val program = IntCodeComputer(instructionData)

    SendSprintScriptToProgram(program, listOf(
        "NOT C T",
        "OR D J",
        "AND T J",
        "NOT A T",
        "OR T J"))

    program.Run()
    var programOutput = ""
    while (program.HasOutput())
    {
        val output = program.GetOutput()
        if (output > 255)
            programOutput += output
        else
            programOutput += output.toChar()
    }
    println(programOutput)
}

fun SendSprintScriptToProgram(program : IntCodeComputer, springScriptLines : List<String>)
{
    for (line in springScriptLines)
    {
        for (c in line)
            program.AddInput(c.toLong())
        program.AddInput(10)
    }
    for (c in "WALK")
        program.AddInput(c.toLong())
    program.AddInput(10)
}
