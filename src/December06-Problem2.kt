package aoc.december6.problem2

import java.io.File

val keyOrbitsValueMap : MutableMap<String, String> = mutableMapOf()

fun main()
{
    val input = File("input/December06-Problem.txt").readLines()
    val parseRegex = Regex("(?<A>.+)\\)(?<B>.+)")

    for (i in input)
    {
        val groups = parseRegex.find(i)?.groups
        val A = groups?.get("A")?.value.orEmpty()
        val B = groups?.get("B")?.value.orEmpty()
        keyOrbitsValueMap[B] = A
    }

    val parentsOfYou = GetParentsOfNode("YOU")
    val parentsOfSanta = GetParentsOfNode("SAN")

    var commonParents = 0
    for (i in 0 until parentsOfYou.size)
        if (parentsOfYou[i] == parentsOfSanta[i])
            commonParents++
        else
            break;

    val requiredTransfers = (parentsOfYou.size - commonParents) + (parentsOfSanta.size - commonParents)
    println("Answer: $requiredTransfers")
}

fun GetParentsOfNode(node : String) : MutableList<String>
{
    val parentNodes : MutableList<String> = mutableListOf()
    var i : String = node
    while (keyOrbitsValueMap.containsKey(i))
    {
        i = keyOrbitsValueMap[i].orEmpty()
        parentNodes.add(0, i)
    }

    return parentNodes
}
