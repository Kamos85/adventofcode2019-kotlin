package aoc.december20.nodeandtransition

import IntVector2

class Node(val position : IntVector2, val name : String, val level : Int = 0, val innerPortal : Boolean = true)
{
    var valid : Boolean = true

    val transitions = mutableListOf<Transition>()

    fun CreateTransitionToNode(step : Int, node : Node)
    {
        val transition = Transition(this, step, node)
        transitions.add(transition)
        node.AddTransitionFromOtherNode(transition)
    }

    fun HasTransitionToNode(node : Node) : Boolean
    {
        return transitions.any { i -> i.TakeTransitionFromNode(this) == node }
    }

    private fun AddTransitionFromOtherNode(transition: Transition)
    {
        transitions.add(transition)
    }

    override fun toString(): String
    {
        return position.toString() + "-" + name
    }
}

class Transition(var node1: Node, var step: Int, var node2: Node)
{
    fun TakeTransitionFromNode(node : Node): Node
    {
        if (node == node1)
            return node2
        if (node == node2)
            return node1
        throw Exception("This transition cannot be taken from the given node")
    }

    override fun toString(): String
    {
        return "$node1 --$step-- $node2"
    }
}
