package aoc.december18.MazeRobot

import IntVector2
import aoc.december15.problem2.directions
import aoc.december18.nodeandtransition.Node

class MazeRobot
{
    val map: MutableList<MutableList<Char>> = mutableListOf()
    var mapWidth = -1;
    var startNode: Node = Node(IntVector2(-1, -1))
    val positionToNode = mutableMapOf<IntVector2, Node>()
    val keyToNode = mutableMapOf<String, Node>()
    var totalKeysToCollect = 0
    var cachedPathDistances: MutableMap<Node, MutableMap<Node, Int>> = mutableMapOf()
    var startPosition = IntVector2(-1, -1)

    enum class Direction(val value: Int, val Vector2: IntVector2)
    {
        North(1, IntVector2(0, -1)),
        South(2, IntVector2(0, 1)),
        West(3, IntVector2(-1, 0)),
        East(4, IntVector2(1, 0));
    }

    fun Setup(quadrantIndex : Int, map : List<String>)
    {
        ParseMap(map)

        startPosition = IntVector2(mapWidth / 2, map.size / 2)
        when(quadrantIndex)
        {
            0 -> startPosition = startPosition + IntVector2(-1,-1)
            1 -> startPosition = startPosition + IntVector2(1,-1)
            2 -> startPosition = startPosition + IntVector2(-1,1)
            3 -> startPosition = startPosition + IntVector2(1,1)
        }

        CreateGraph()

        positionToNode.forEach { i ->
            run {
                if (i.value.key.isNotEmpty())
                {
                    totalKeysToCollect++
                    keyToNode[i.value.key] = i.value
                }
            }
        }
        println("totalKeysToCollect: $totalKeysToCollect")

        PrintScreenWithNodes()
    }

    fun ParseMap(stringMap : List<String>)
    {
        for (i in stringMap)
            map.add(i.toCharArray().toMutableList())
        mapWidth = map.last().size
    }

    val visitedGraphPositions = mutableSetOf<IntVector2>()
    fun CreateGraph()
    {
        processMapEntry(null, startPosition, 0)
        startNode = positionToNode[startPosition] ?: throw Exception("No start node")
    }

    fun processMapEntry(fromNode: Node?, position: IntVector2, steps: Int)
    {
        visitedGraphPositions.add(position)

        val nextOpenPositions = mutableListOf<IntVector2>()
        for (direction in directions)
        {
            val nextPos = position + direction.Vector2
            val nextPosChar = map[nextPos.y][nextPos.x]
            if (nextPosChar == '#')
                continue
            if (visitedGraphPositions.contains(nextPos))
                continue
            nextOpenPositions.add(nextPos)
        }

        val posChar = map[position.y][position.x]
        if (posChar != '.' || nextOpenPositions.size >= 2)
        { // create a node
            val newNode = Node(position)
            if (posChar != '.' && posChar != '@')
            {
                val asciiChar = map[position.y][position.x].toChar()
                if (asciiChar.toInt() in 65..90)
                    newNode.door = asciiChar.toString()
                if (asciiChar.toInt() in 97..122)
                    newNode.key = asciiChar.toString()
            }
            for (next in nextOpenPositions)
                processMapEntry(newNode, next, 1)
            if (posChar == '.' && newNode.GetTransitionCount() == 0)
                return  // this node has no use (leads to empty paths)
            if (posChar == '.' && newNode.GetTransitionCount() == 1)
            { // this node only links to the next one and should not be a node
                val transition = newNode.GetOnlySingleTransition()
                fromNode?.CreateTransitionToNode(steps + transition.step, transition.TakeTransitionFromNode(newNode))
                return
            }
            positionToNode[position] = newNode
            fromNode?.CreateTransitionToNode(steps, newNode)
        } else
        {
            if (nextOpenPositions.size == 1)
                processMapEntry(fromNode, nextOpenPositions[0], steps + 1)
        }
    }

    val visitedNodesForKeys = mutableSetOf<Node>()
    fun GetDirectUncollectedAccessibleKeys(collectedKeys: MutableSet<String>): MutableSet<String>
    {
        val accessibleKeys = mutableSetOf<String>()

        visitedNodesForKeys.clear()
        VisitNodeForKeys(startNode, collectedKeys, accessibleKeys)
        return accessibleKeys
    }

    fun VisitNodeForKeys(node: Node, collectedKeys: MutableSet<String>, accessibleKeys: MutableSet<String>)
    {
        visitedNodesForKeys.add(node)

        if (node.key.isNotEmpty())
        {
            if (!collectedKeys.contains(node.key))
            {
                accessibleKeys.add(node.key)
                return
            }
        }

        if (node.door.isNotEmpty())
        {
            val key = node.door.toLowerCase()
            if (!collectedKeys.contains(key))
                return
        }

        for (transition in node.transitions)
        {
            val otherNode = transition.TakeTransitionFromNode(node)
            if (!visitedNodesForKeys.contains(otherNode))
                VisitNodeForKeys(otherNode, collectedKeys, accessibleKeys)
        }
    }

    fun GetStepsToNode(fromNode: Node, targetNode: Node) : Int
    {
        if (cachedPathDistances.containsKey(fromNode))
        {
            val toNodes = cachedPathDistances[fromNode] ?: throw Exception("Can't happen")
            val result = toNodes[targetNode]
            if (result != null)
                return result
        }

        // Dijkstra's shortest path algorithm implemented from Wikipedia's pseudocode
        val Q: MutableSet<Node> = mutableSetOf()
        val distNodes: MutableMap<Node, Int> = mutableMapOf()
        val prevNodes: MutableMap<Node, Node?> = mutableMapOf()

        positionToNode.forEach { i ->
            run {
                distNodes[i.value] = Int.MAX_VALUE
                prevNodes[i.value] = null
                Q.add(i.value)
            }
        }

        distNodes[fromNode] = 0

        while (Q.isNotEmpty())
        {
            val u = Q.minBy { i -> distNodes.getOrDefault(i, Int.MAX_VALUE) }
                ?: throw Exception("Cannot happen as Q not empty")
            Q.remove(u)

            val distanceU = distNodes[u] ?: throw Exception("target node has no distance set")

            if (u == targetNode)
            {
                if (!cachedPathDistances.containsKey(fromNode))
                    cachedPathDistances[fromNode] = mutableMapOf()
                val toNodes = cachedPathDistances[fromNode] ?: throw Exception("Can't happen")
                toNodes[targetNode] = distanceU
                return distanceU
            }

            for (transition in u.transitions)
            {
                val v = transition.TakeTransitionFromNode(u)
                if (!Q.contains(v))
                    continue
                val alt = distanceU + transition.step
                val distanceV = distNodes[v] ?: throw Exception("node v no distance set")
                if (alt < distanceV)
                {
                    distNodes[v] = alt
                    prevNodes[v] = u
                }
            }
        }
        throw Exception("No path between $fromNode and $targetNode")
    }

    fun GetStartNode() : Node
    {
        return startNode
    }

    fun GetNodeForKey(key : String) : Node?
    {
        return keyToNode[key]
    }

    fun PrintScreenWithNodes()
    {
        for (y in 0 until map.size)
        {
            for (x in 0 until mapWidth)
            {
                if (positionToNode.containsKey(IntVector2(x, y)))
                    print(ConsoleColors.RED_BACKGROUND + map[y][x])
                else
                    print(ConsoleColors.RESET + map[y][x])
            }
            println(ConsoleColors.RESET)
        }
    }
}
