package aoc.december8.problem2

import java.io.File

enum class PixelType(val value: Int, val stringRepr : String)
{
    BLACK(0, ConsoleColors.BLACK_BACKGROUND + " "),
    WHITE(1, ConsoleColors.WHITE_BACKGROUND + "X"),
    TRANSPARENT(2, "?")
}

val imageWidth = 25
val imageHeight = 6
val layers : MutableList<MutableList<MutableList<Int>>> = mutableListOf()
val image : MutableList<MutableList<String>> = mutableListOf()

fun main()
{
    Setup()
    ComposeImage()

    println("Answer:")
    image.forEach { i -> println(i.joinToString("")) }
}

fun Setup()
{
    val input: String = File("input/December08-Problem.txt").readText()
    var layerIndex = 1

    var parseIndex = 0
    while (parseIndex < input.length)
    {
        layers.add(mutableListOf(mutableListOf()))
        for (y in 0 until imageHeight)
        {
            if (y > 0) // dont add row for the first row, it's already there
                layers.last().add(mutableListOf())
            for (x in 0 until imageWidth)
            {
                layers.last().last().add(input[parseIndex].toString().toInt())
                parseIndex++
            }
        }
    }
}

fun ComposeImage()
{
    for (y in 0 until imageHeight)
    {
        image.add(mutableListOf())
        for (x in 0 until imageWidth)
        {
            val character =
                when(GetImageDigitAt(y, x))
                {
                    PixelType.BLACK.value -> PixelType.BLACK.stringRepr
                    PixelType.WHITE.value -> PixelType.WHITE.stringRepr
                    PixelType.TRANSPARENT.value -> PixelType.TRANSPARENT.stringRepr
                    else -> "?"
                }
            image.last().add(character)
        }
    }
}

fun GetImageDigitAt(y : Int, x : Int) : Int
{
    for (layer in layers)
    {
        when (val d = layer[y][x])
        {
            PixelType.BLACK.value -> return d
            PixelType.WHITE.value -> return d
            PixelType.TRANSPARENT.value -> d
        }
    }
    return PixelType.TRANSPARENT.value
}
