package aoc.december16.problem2

import java.io.File
import java.lang.Exception

const val phaseCount = 100

var ciphers = mutableListOf<Int>()

fun main()
{
    //val readInput = File("input/temp.txt").readText()
    val readInput = File("input/December16-Problem.txt").readText()
    var input = readInput
    val messageOffset = input.substring(0,7).toInt()

    println("messageOffset: $messageOffset")

    for (i in 1 until 10000)
        input += readInput
    println("original size: " +  input.length)
    if (input.length % 2 == 1)
        throw Exception("Need to take care of this case");

    val half = input.length/2
    if (messageOffset < half)
        throw Exception("Solution only works if the message is in the lower half of the input");

    input = input.substring(messageOffset)

    ciphers = (input.map { i -> i.toString().toInt() }).toMutableList()

    println("size after: " + ciphers.size)

    for (i in 1..phaseCount)
        ProcessLatestHalf();

    println("Answer: ${ciphers.joinToString("").subSequence(0,8)}")
}

fun ProcessLatestHalf()
{
    // based on the observation that the latter half processing is simply adding numbers together
    var sum = 0
    for (i in (ciphers.size - 1) downTo 0)
    {
        sum += ciphers[i]
        ciphers[i] = sum % 10
    }
}
