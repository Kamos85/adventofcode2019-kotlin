package aoc.december17.problem1

import IntVector2
import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File

val map : MutableList<MutableList<Char>> = mutableListOf()
val intersections : MutableSet<IntVector2> = mutableSetOf()

fun main()
{
    val input: String = File("input/December17-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    val program = IntCodeComputer(instructionData)

    map.add(mutableListOf())
    program.Run()
    while (program.HasOutput())
    {
        val outputChar = program.GetOutput()
        if (outputChar == 10L)
            map.add(mutableListOf())
        else
            map.last().add(outputChar.toChar())
    }
    // remove last two entries, they are empty
    map.removeAt(map.size - 1)
    map.removeAt(map.size - 1)

    PrintScreen()
    FindIntersections()

    val answer = intersections.fold(0, {sum, i -> sum + i.x*i.y})

    println("Answer: $answer")
}

fun PrintScreen()
{
    map.forEach { i -> println(i.joinToString("")) }
}

fun FindIntersections()
{
    var sum = 0
    val mapWidth = map.last().size
    for (y in 1 until map.size-1)
    {
        for (x in 1 until mapWidth-1)
        {
            if (map[y][x] == '#' &&
                map[y-1][x] == '#' &&
                map[y+1][x] == '#' &&
                map[y][x-1] == '#' &&
                map[y][x+1] == '#')
            {
                intersections.add(IntVector2(x,y))
            }
        }
    }
}
