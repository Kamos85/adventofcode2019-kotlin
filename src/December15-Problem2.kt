package aoc.december15.problem2

import IntVector2
import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File
import java.lang.Integer.max
import java.lang.Integer.min
import java.util.*

enum class Direction(val value: Int, val Vector2 : IntVector2)
{
    North(1, IntVector2(0,-1)),
    South(2, IntVector2(0,1)),
    West(3, IntVector2(-1,0)),
    East(4, IntVector2(1,0));

    companion object
    {
        fun Opposite(direction : Direction) : Direction
        {
            return when (direction)
            {
                North -> South
                South -> North
                West -> East
                East -> West
            }
        }
    }
}

enum class State(val value: Int, val strRep : String)
{
    Wall(0, "#"),
    Empty(1, "."),
    Oxygen(2, "O")
}

val map = mutableMapOf<IntVector2, State>()
val directions = listOf(Direction.North, Direction.South, Direction.West, Direction.East)
var currentPos = IntVector2(0,0)
var program : IntCodeComputer? = null

fun main()
{
    val input: String = File("input/December15-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    program = IntCodeComputer(instructionData)
    // get robot to oxygen
    println(GetMaxStepsMadeMovingRobotAround(true))
    // reset state (forget map) and get max steps made from oxygen
    map.clear()
    val maxSteps = GetMaxStepsMadeMovingRobotAround(false)

    println("Answer: $maxSteps")
}

fun GetMaxStepsMadeMovingRobotAround(stopWhenAtOxygen : Boolean) : Int
{
    val pathToStart : Stack<Direction> = Stack()
    var maxSteps = 0

    var madeStep = false
    do
    {
        maxSteps = max(maxSteps, pathToStart.size)
        madeStep = false
        for (direction in directions)
        {
            var nextPos = currentPos + direction.Vector2
            if (map.containsKey(nextPos))
                continue // already visited
            val state = DoRobotStep(direction)
            map[nextPos] = state
            if (state == State.Wall)
                continue // could not do step
            // here we could do a step, so update
            pathToStart.push(Direction.Opposite(direction))
            currentPos += direction.Vector2
            madeStep = true
            if (stopWhenAtOxygen && state == State.Oxygen)
                return maxSteps
            break
        }

        if (!madeStep)
        { // could not make a step on current position, backtrack
            if (pathToStart.isNotEmpty())
            {
                val direction = pathToStart.pop()
                DoRobotStep(direction)
                currentPos += direction.Vector2
                madeStep = true
            }
        }
    } while (madeStep)

    return maxSteps
}

fun DoRobotStep(direction : Direction) : State
{
    program?.AddInput(direction.value.toLong())
    program?.Run()
    val output = program?.GetOutput()
    if (output == null)
        throw Exception("Should never happen")
    return when (output.toInt())
    {
        State.Wall.value -> State.Wall
        State.Empty.value -> State.Empty
        State.Oxygen.value -> State.Oxygen
        else -> throw Exception("Unknown State ${output.toInt()}")
    }
}

fun PrintScreen()
{
    var minx = 9999
    var maxx = -9999
    var miny = 9999
    var maxy = -9999
    map.forEach {
            i ->
        run {
            minx = min(minx, i.key.x)
            maxx = max(maxx, i.key.x)
            miny = min(miny, i.key.y)
            maxy = max(maxy, i.key.y)
        }
    }

    for (y in miny..maxy)
    {
        for (x in minx..maxx)
        {
            if (x == 0 && y == 0)
            {
                print("S")
                continue
            }
            if (map.containsKey(IntVector2(x, y)))
                print(map[IntVector2(x,y)]?.strRep)
            else
                print(State.Wall.strRep)
        }
        println()
    }
}
