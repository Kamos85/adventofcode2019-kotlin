// Approach to this problem:
// Add extra required check and do the computation

package aoc.december4.problem2

import java.io.File

val ciphers = mutableListOf(0,0,0,0,0,0)
val endCiphers = mutableListOf(0,0,0,0,0,0)

fun main()
{
    val input: String = File("input/December04-Problem.txt").readText()
    val parseRegex = Regex("(?<LowBound>\\d+)-(?<HighBound>\\d+)")
    val groups = parseRegex.find(input)?.groups
    val lowBound = groups?.get("LowBound")?.value.orEmpty()
    val highBound = groups?.get("HighBound")?.value.orEmpty()

    for ((index, c) in lowBound.withIndex())
        ciphers[index] = c.toString().toInt()
    for ((index, c) in highBound.withIndex())
        endCiphers[index] = c.toString().toInt()

    SetCiphersToIncreasingOrder()

    var validSolutions = 0
    do {
        validSolutions += if (CheckIfSeparateDoubleExist()) 1 else 0
        IncreaseCiphersAtIndex(5)
    } while (IsCiphersBelowHighBound())

    println("Answer: " + validSolutions)
}

fun SetCiphersToIncreasingOrder()
{
    for (i in 1..5)
        if (ciphers[i] < ciphers[i-1])
            ciphers[i] = ciphers[i-1]
}

fun CheckIfSeparateDoubleExist() : Boolean
{
    for (i in 0..4)
    {
        val c1 = ciphers.getOrElse(i-1) {-1}
        val c2 = ciphers.getOrElse(i) {-1}
        val c3 = ciphers.getOrElse(i+1) {-1}
        val c4 = ciphers.getOrElse(i+2) {-1}
        if (c2==c3 && c1 != c2 && c3 != c4)
            return true
    }
    return false
}

fun IncreaseCiphersAtIndex(index : Int)
{
    ciphers[index] += 1
    if (ciphers[index] > 9)
    {
        IncreaseCiphersAtIndex(index - 1)
        ciphers[index] = ciphers[index - 1]
    }
}

fun IsCiphersBelowHighBound() : Boolean
{
    for (i in 0..5)
    {
        if (ciphers[i] < endCiphers[i])
            return true
        if (ciphers[i] > endCiphers[i])
            return false
    }
    return true
}
