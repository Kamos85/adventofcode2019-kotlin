// Approach to this problem:
// Create a 2D field (list of lists)
// Expand the field as necessary, if negative indices are required, add new entries at the start
// Find the closest intersection by going over the grid
// In hindsight this solution was much easier to solve using a set of IntVector2 to represent a wire
// then the intersection of the two sets gives the intersection of the wires

package aoc.december3.problem1

import java.io.File

val parseRegex = Regex("(?<Direction>[URDL])(?<Steps>\\d+)")

val wireField : MutableList<MutableList<Int>> = mutableListOf()
val wireFieldSizeX: Int
    get() = if (wireField.size > 0) { wireField[wireField.size-1].size } else { 0 }
val wireFieldSizeY: Int
    get() = if (wireField.size > 0) { wireField.size } else { 0 }
var wireCenterX = 0
var wireCenterY = 0

enum class Direction(val abbreviation : String, val xDir : Int, val yDir : Int)
{
    Left("L", -1, 0),
    Right("R", 1, 0),
    Up("U", 0, -1),
    Down("D", 0, 1);

    companion object
    {
        fun Convert(str : String) : Direction
        {
            return when (str)
            {
                Left.abbreviation -> Left
                Right.abbreviation -> Right
                Up.abbreviation -> Up
                Down.abbreviation -> Down
                else -> { throw IllegalArgumentException("'$str' is not a member of enum Direction")}
            }
        }
    }
}

fun main()
{
    val input: List<String> = File("input/December03-Problem.txt").readLines()
    val wire1Data: List<String> = input[0].split(",")
    val wire2Data: List<String> = input[1].split(",")
    wireField.add(mutableListOf(0))
    SetWireDataIntoField(wire1Data, 1)
    SetWireDataIntoField(wire2Data, 2)
    val answer = FindClosestIntersectionOfTwoWireIDs(1, 2)
    println("Answer: $answer")
}

fun SetWireDataIntoField(wireData : List<String>, wireID: Int)
{
    var wireLocalPosX : Int = 0
    var wireLocalPosY : Int = 0
    for (wireStep in wireData)
    {
        val groups = parseRegex.find(wireStep)?.groups
        val directionString = groups?.get("Direction")?.value.orEmpty()
        val stepsString = groups?.get("Steps")?.value.orEmpty()
        val direction = Direction.Convert(directionString)
        val steps = stepsString.toInt()
        EnsureWireFieldIsBigEnough(wireLocalPosX + wireCenterX, wireLocalPosY + wireCenterY, direction, steps)
        AddWireData(wireLocalPosX + wireCenterX, wireLocalPosY + wireCenterY, direction, steps, wireID)
        wireLocalPosX += direction.xDir * steps
        wireLocalPosY += direction.yDir * steps
    }
}

fun EnsureWireFieldIsBigEnough(x : Int, y : Int, direction : Direction, steps : Int)
{
    val extendBy = when (direction)
    {
        Direction.Down -> (y + steps) - (wireFieldSizeY - 1)
        Direction.Up -> -(y - steps)
        Direction.Right -> (x + steps) - (wireFieldSizeX - 1)
        Direction.Left -> -(x - steps)
    }

    if (extendBy > 0)
        ExtendWireField(direction, extendBy)
}

fun ExtendWireField(direction : Direction, value : Int)
{
    when (direction)
    {
        Direction.Up -> for (i in 0 until value) { wireField.add(0, MutableList(wireFieldSizeX) {0})}
        Direction.Down -> for (i in 0 until value) { wireField.add(wireFieldSizeY, MutableList(wireFieldSizeX) {0})}
        Direction.Left -> for (i in 0 until wireFieldSizeY) { wireField[i].addAll(0, MutableList(value) {0})}
        Direction.Right -> for (i in 0 until wireFieldSizeY) { wireField[i].addAll(wireFieldSizeX, MutableList(value) {0})}
    }

    when (direction)
    {
        Direction.Up -> wireCenterY += value
        Direction.Left -> wireCenterX += value
        else -> {}
    }
}

fun AddWireData(x : Int, y : Int, direction : Direction, steps : Int, wireID: Int)
{
    for (i in 1..steps)
        wireField[y + direction.yDir * i][x + direction.xDir * i] = wireField[y + direction.yDir * i][x + direction.xDir * i] or (1 shl (wireID-1))
}

fun FindClosestIntersectionOfTwoWireIDs(wire1ID: Int, wire2ID: Int) : Int
{
    val intersectionValue = (1 shl (wire1ID-1)) or (1 shl (wire2ID-1))
    var closestIntersectionLength = Int.MAX_VALUE
    for (y in 0 until wireFieldSizeY)
    {
        for (x in 0 until wireFieldSizeX)
        {
            if (wireField[y][x] and intersectionValue == intersectionValue)
            {
                val manhattanLength = kotlin.math.abs(x - wireCenterX) + kotlin.math.abs(y - wireCenterY)
                if (manhattanLength != 0)
                    closestIntersectionLength = kotlin.math.min(closestIntersectionLength, manhattanLength)
            }
        }
    }
    return closestIntersectionLength;
}
