package aoc.december14.reaction

class Reaction(val outputProduct : String, val outputAmount : Int)
{
    val inputs = mutableMapOf<String, Int>()

    fun AddInput(product : String, amount : Int)
    {
        inputs[product] = amount
    }

    override fun toString(): String
    {
        var reactionStr = ""
        inputs.forEach { i -> reactionStr += (i.value.toString() + " " + i.key + ", ") }
        reactionStr += " -> $outputAmount $outputProduct"
        return reactionStr;
    }
}
