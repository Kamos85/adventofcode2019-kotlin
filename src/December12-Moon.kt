package aoc.december12.moon

import IntVector3
import java.lang.Math.abs

class Moon(var position : IntVector3)
{
    var velocity = IntVector3(0,0,0)

    fun ApplyGravityBetweenThisAndOtherMoon(other : Moon)
    {
        if (position.x < other.position.x)
        {
            velocity.x += 1
            other.velocity.x -= 1
        } else
        {
            if (position.x > other.position.x)
            {
                velocity.x -= 1
                other.velocity.x += 1
            }
        }

        if (position.y < other.position.y)
        {
            velocity.y += 1
            other.velocity.y -= 1
        } else
        {
            if (position.y > other.position.y)
            {
                velocity.y -= 1
                other.velocity.y += 1
            }
        }

        if (position.z < other.position.z)
        {
            velocity.z += 1
            other.velocity.z -= 1
        } else
        {
            if (position.z > other.position.z)
            {
                velocity.z -= 1
                other.velocity.z += 1
            }
        }
    }

    fun UpdatePosition()
    {
        position += velocity
    }

    fun GetTotalEnergy() : Int
    {
        val potentialEnergy = abs(position.x) + abs(position.y) + abs(position.z)
        val kineticEnergy = abs(velocity.x) + abs(velocity.y) + abs(velocity.z)
        return potentialEnergy * kineticEnergy
    }

    override fun equals(other: Any?): Boolean
    {
        if (other == null || other !is Moon) return false
        return position == other.position && velocity == other.velocity
    }

    override fun hashCode(): Int
    {
        var hashCode = 1861411795;
        hashCode = hashCode * -1521134295 + position.hashCode();
        hashCode = hashCode * -1521134295 + velocity.hashCode();
        return hashCode;
    }

    override fun toString(): String
    {
        return "(P: $position , V: $velocity)";
    }
}
