package aoc.december12.problem1

import IntVector3
import aoc.december12.moon.Moon
import java.io.File

val moons : MutableList<Moon> = mutableListOf()
val stepsToSimulate = 1000

fun main()
{
    val input = File("input/December12-Problem.txt").readLines()
    val parseRegex = Regex("<x=(?<x>-?\\d+), y=(?<y>-?\\d+), z=(?<z>-?\\d+)>")
    for (i in input)
    {
        val groups = parseRegex.find(i)?.groups
        val xStr = groups?.get("x")?.value.orEmpty()
        val yStr = groups?.get("y")?.value.orEmpty()
        val zStr = groups?.get("z")?.value.orEmpty()
        moons.add(Moon(IntVector3(xStr.toInt(), yStr.toInt(), zStr.toInt())))
    }

    for (i in 1..stepsToSimulate)
    {
        UpdateMoonVelocities()
        UpdateMoonPositions()
    }

    val totalEnergy = moons.fold(0, {sum,i -> sum + i.GetTotalEnergy()})
//    for (m in moons)
//        println(m)

    println("Answer: $totalEnergy")
}

fun UpdateMoonVelocities()
{
    for (i in 0 until moons.size)
        for (j in i+1 until moons.size)
            moons[i].ApplyGravityBetweenThisAndOtherMoon(moons[j])
}

fun UpdateMoonPositions()
{
    for (i in moons)
        i.UpdatePosition()
}
