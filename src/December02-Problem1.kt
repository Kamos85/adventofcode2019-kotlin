// Approach to this problem:
// Implement the required instructions and execute the program

package aoc.december2.problem1

import java.io.File

var instructionData: MutableList<Int> = mutableListOf()

enum class Instruction(val value: Int) {
    ADD(1),
    MULTIPLY(2),
    STOP(99)
}

fun main()
{
    val input: String = File("input/December02-Problem.txt").readText()
    instructionData = input.split(",").map { it.toInt() }.toMutableList()

    // apply instruction as given in problem
    instructionData[1] = 12
    instructionData[2] = 2

    for (x in 0 until instructionData.size step 4)
    {
        if (instructionData[x] == Instruction.STOP.value)
            break;
        handleInstruction(instructionData[x], instructionData[x+1], instructionData[x+2], instructionData[x+3])
    }

    println("Answer: ${instructionData[0]}")
}

fun handleInstruction(instructionType : Int, inputOperand1 : Int, inputOperand2 : Int, outputOperand : Int)
{
    when (instructionType)
    {
        Instruction.ADD.value -> instructionData[outputOperand] = instructionData[inputOperand1] + instructionData[inputOperand2]
        Instruction.MULTIPLY.value -> instructionData[outputOperand] = instructionData[inputOperand1] * instructionData[inputOperand2]
        else -> println("Unknown instruction: $instructionType")
    }
}
