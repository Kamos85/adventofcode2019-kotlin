// Approach to this problem:
// Copy code from day 2 and expand on it
// Add a new list to keep track of the different parameter modes and implement the modes per the requirements
// Run code

package aoc.december5.problem1

import java.io.File

var instructionData: MutableList<Int> = mutableListOf()

val parameterModes = mutableListOf(0,0,0)

var instructionPointer = 0
var input = 1 // for input
var outputs = mutableListOf<Int>()

enum class Instruction(val value: Int) {
    ADD(1),
    MULTIPLY(2),
    INPUT(3),
    OUTPUT(4),
    STOP(99)
}

fun main()
{
    val input: String = File("input/December05-Problem.txt").readText()
    //val input = "1101,100,-1,4,0"
    instructionData = input.split(",").map { it.toInt() }.toMutableList()

    while (HandleInstruction())
        ;

    println(outputs)
    println("Answer: ${outputs.last()}")
}

fun HandleInstruction() : Boolean
{
    val opcodeWithParameterModes = instructionData[instructionPointer]
    val opcode = opcodeWithParameterModes % 100
    val parameterModes = opcodeWithParameterModes / 100
    SetParameterModes(parameterModes)

    when (opcode)
    {
        Instruction.ADD.value -> Instruction_Add()
        Instruction.MULTIPLY.value -> Instruction_Multiply()
        Instruction.INPUT.value -> Instruction_Input()
        Instruction.OUTPUT.value -> Instruction_Output()
        Instruction.STOP.value -> return false;
        else -> println("Unknown instruction: $opcode")
    }
    return true
}

fun Instruction_Add()
{
    val value1 = GetValueFromParameter(1)
    val value2 = GetValueFromParameter(2)
    val value3 = instructionData[instructionPointer+3]
    instructionData[value3] = value1 + value2
    instructionPointer += 4
}

fun GetValueFromParameter(parameterIndex : Int) : Int
{
    val parameterMode = parameterModes[parameterIndex-1]
    val operandValue = instructionData[instructionPointer+parameterIndex]
    if (parameterMode == 0)
        return instructionData[operandValue]
    else
        return operandValue
}

fun Instruction_Multiply()
{
    val value1 = GetValueFromParameter(1)
    val value2 = GetValueFromParameter(2)
    val value3 = instructionData[instructionPointer+3]
    instructionData[value3] = value1 * value2
    instructionPointer += 4
}

fun Instruction_Input()
{
    val value1 = instructionData[instructionPointer+1]
    instructionData[value1] = input
    instructionPointer += 2
}

fun Instruction_Output()
{
    val value1 = GetValueFromParameter(1)
    outputs.add(value1)
    instructionPointer += 2
}

fun SetParameterModes(parameters : Int)
{
    ResetParameterModes()
    var params = parameters
    var index = 0
    while (params > 0)
    {
        parameterModes[index] = params % 10
        if (params % 10 >= 2)
            println("SHOULD NOT HAPPEN!") // sanity test
        params /= 10
        index++
    }
}

fun ResetParameterModes()
{
    for (i in parameterModes.indices)
        parameterModes[i] = 0
}
