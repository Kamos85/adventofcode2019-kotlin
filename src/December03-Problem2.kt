// Approach to this problem:
// Go over the two wires and keep track of the steps in their intersections
// Find the intersection with the lowest combined steps

package aoc.december3.problem2

import java.io.File

val parseRegex = Regex("(?<Direction>[URDL])(?<Steps>\\d+)")

val wireField : MutableList<MutableList<Int>> = mutableListOf()
val wireFieldSizeX: Int
    get() = if (wireField.size > 0) { wireField[wireField.size-1].size } else { 0 }
val wireFieldSizeY: Int
    get() = if (wireField.size > 0) { wireField.size } else { 0 }
var wireCenterX = 0
var wireCenterY = 0

class Intersection(var x : Int, var y : Int, var wireSteps1 : Int, var wireSteps2 : Int)
{
    fun GetCombinedSteps() : Int
    {
        return wireSteps1 + wireSteps2
    }

    fun SetStepIfNotSetFromWireID(steps : Int, wireID: Int)
    {
        if (wireID == 1)
            if (wireSteps1 == 0)
                wireSteps1 = steps
        if (wireID == 2)
            if (wireSteps2 == 0)
                wireSteps2 = steps
    }
}

val interSectionList = mutableListOf<Intersection>()
fun GetIntersectionInList(x : Int, y : Int) : Intersection?
{
    return interSectionList.find { i -> i.x == x && i.y == y }
}

enum class Direction(val abbreviation : String, val xDir : Int, val yDir : Int)
{
    Left("L", -1, 0),
    Right("R", 1, 0),
    Up("U", 0, -1),
    Down("D", 0, 1);

    companion object
    {
        fun Convert(str : String) : Direction
        {
            return when (str)
            {
                Left.abbreviation -> Left
                Right.abbreviation -> Right
                Up.abbreviation -> Up
                Down.abbreviation -> Down
                else -> { throw IllegalArgumentException("'$str' is not a member of enum Direction")}
            }
        }
    }
}

fun main()
{
    val input: List<String> = File("input/December03-Problem.txt").readLines()
    //val input = listOf("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")
    val wire1Data: List<String> = input[0].split(",")
    val wire2Data: List<String> = input[1].split(",")
    wireField.add(mutableListOf(0))
    SetWireDataIntoField(wire1Data, 1)
    SetWireDataIntoField(wire2Data, 2)
    GoOverWireForIntersection(wire1Data, 1)
    GoOverWireForIntersection(wire2Data, 2)
    val answer = FindLowestStepIntersection()
    println("Answer: $answer")
}

fun SetWireDataIntoField(wireData : List<String>, wireID: Int)
{
    var wireLocalPosX : Int = 0
    var wireLocalPosY : Int = 0
    for (wireStep in wireData)
    {
        val groups = parseRegex.find(wireStep)?.groups
        val directionString = groups?.get("Direction")?.value.orEmpty()
        val stepsString = groups?.get("Steps")?.value.orEmpty()
        val direction = Direction.Convert(directionString)
        val steps = stepsString.toInt()
        EnsureWireFieldIsBigEnough(wireLocalPosX + wireCenterX, wireLocalPosY + wireCenterY, direction, steps)
        AddWireData(wireLocalPosX + wireCenterX, wireLocalPosY + wireCenterY, direction, steps, wireID)
        wireLocalPosX += direction.xDir * steps
        wireLocalPosY += direction.yDir * steps
    }
}

fun GoOverWireForIntersection(wireData : List<String>, wireID: Int)
{
    val intersectionValue = 3 // (wire ID 1 and 2 cross)
    var wireLocalPosX : Int = 0
    var wireLocalPosY : Int = 0
    var stepsMade = 0
    for (wireStep in wireData)
    {
        val groups = parseRegex.find(wireStep)?.groups
        val directionString = groups?.get("Direction")?.value.orEmpty()
        val stepsString = groups?.get("Steps")?.value.orEmpty()
        val direction = Direction.Convert(directionString)
        val steps = stepsString.toInt()

        val wireGlobalPosX = wireLocalPosX + wireCenterX
        val wireGlobalPosY = wireLocalPosY + wireCenterY
        for (i in 1..steps)
        {
            stepsMade += 1
            val stepPositionX = wireGlobalPosX + direction.xDir * i
            val stepPositionY = wireGlobalPosY + direction.yDir * i
            if (wireField[stepPositionY][stepPositionX] == intersectionValue)
            {
                var intersection = GetIntersectionInList(stepPositionX, stepPositionY)
                if (intersection == null)
                {
                    intersection = Intersection(stepPositionX, stepPositionY, 0, 0)
                    interSectionList.add(intersection)
                }
                intersection.SetStepIfNotSetFromWireID(stepsMade, wireID)
            }
        }

        wireLocalPosX += direction.xDir * steps
        wireLocalPosY += direction.yDir * steps
    }
}


fun EnsureWireFieldIsBigEnough(x : Int, y : Int, direction : Direction, steps : Int)
{
    val extendBy = when (direction)
    {
        Direction.Down -> (y + steps) - (wireFieldSizeY - 1)
        Direction.Up -> -(y - steps)
        Direction.Right -> (x + steps) - (wireFieldSizeX - 1)
        Direction.Left -> -(x - steps)
    }

    if (extendBy > 0)
        ExtendWireField(direction, extendBy)
}

fun ExtendWireField(direction : Direction, value : Int)
{
    when (direction)
    {
        Direction.Up -> for (i in 0 until value) { wireField.add(0, MutableList(wireFieldSizeX) {0})}
        Direction.Down -> for (i in 0 until value) { wireField.add(wireFieldSizeY, MutableList(wireFieldSizeX) {0})}
        Direction.Left -> for (i in 0 until wireFieldSizeY) { wireField[i].addAll(0, MutableList(value) {0})}
        Direction.Right -> for (i in 0 until wireFieldSizeY) { wireField[i].addAll(wireFieldSizeX, MutableList(value) {0})}
    }

    when (direction)
    {
        Direction.Up -> wireCenterY += value
        Direction.Left -> wireCenterX += value
        else -> {}
    }
}

fun AddWireData(x : Int, y : Int, direction : Direction, steps : Int, wireID: Int)
{
    for (i in 0..steps)
        wireField[y + direction.yDir * i][x + direction.xDir * i] = wireField[y + direction.yDir * i][x + direction.xDir * i] or (1 shl (wireID-1))
}

fun FindLowestStepIntersection() : Int?
{
    return interSectionList.minBy { i -> i.GetCombinedSteps() }?.GetCombinedSteps()
}

fun printWireField()
{
    for (row in wireField)
        println(row)
}
