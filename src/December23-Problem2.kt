// Approach to this problem:
// Simply follow the instructions
// Have 50 instances of the program and have a bit of code that handles the sending/receiving of data between programs
// Save the last value being sent to the NAT at address 255
// Record which Y-values have been sent by the NAT

package aoc.december23.problem2

import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File

val computers : MutableList<IntCodeComputer> = mutableListOf()
const val computerCount = 50

var NATpacketX = -1L
var NATpacketY = -1L
val seenNATPacketYValues = mutableSetOf<Long>()

fun main()
{
    val input: String = File("input/December23-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    for (i in 0 until computerCount)
    {
        computers.add(IntCodeComputer(instructionData))
        computers.last().AddInput(i.toLong())
    }

    var isNetworkIdleRuns = 0
    var idleComputer = 0
    while (true)
    {
        idleComputer = 0
        for (computer in computers)
        {
            computer.Run()
            if (!computer.IsComplete())
            {
                computer.AddInput(-1)
                idleComputer++
            }
            while (computer.HasOutput())
            {
                val address = computer.GetOutput().toInt()
                val x = computer.GetOutput()
                val y = computer.GetOutput()
                if (address == 255)
                {
                    NATpacketX = x
                    NATpacketY = y
                }
                else
                {
                    computers[address].AddInput(x)
                    computers[address].AddInput(y)
                }
            }
        }

        if (idleComputer == computerCount)
            isNetworkIdleRuns++
        else
            isNetworkIdleRuns = 0

        val isNetworkIdle = NATpacketX != -1L && NATpacketY != -1L && isNetworkIdleRuns > 5

        if (isNetworkIdle)
        {
            println("-- NETWORK IDLE -- Sending to address: 0 , x: $NATpacketX , y: $NATpacketY")
            if (seenNATPacketYValues.contains(NATpacketY))
            {
                println("Answer: $NATpacketY")
                return
            }
            seenNATPacketYValues.add(NATpacketY)
            computers[0].AddInput(NATpacketX)
            computers[0].AddInput(NATpacketY)
            isNetworkIdleRuns = 0
        }
    }
}
