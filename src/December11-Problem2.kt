package aoc.december11.problem2

import IntVector2
import aoc.december9.intcodecomputer.IntCodeComputer
import java.io.File

val panelField : MutableList<MutableList<Int>> = mutableListOf()
val panelWidth = 100
val panelHeight = 20
var robotPosition = IntVector2(panelWidth / 2, panelHeight / 2)
var robotDirection = IntVector2(0, -1)
val paintedPositions = mutableSetOf<IntVector2>()

fun main()
{
    val input: String = File("input/December11-Problem.txt").readText()
    val instructionData = input.split(",").map { it.toLong() }.toMutableList()
    val program = IntCodeComputer(instructionData)
    SetupPanelField()

    panelField[robotPosition.y][robotPosition.x] = 1

    do
    {
        val color = GetPanelColorAtRobotPosition().toLong()
        program.AddInput(color)
        program.Run()
        val newColor = program.GetOutput()
        val direction = program.GetOutput()
        panelField[robotPosition.y][robotPosition.x] = newColor.toInt()
        if (color != newColor)
            paintedPositions.add(robotPosition)
        when (direction)
        {
            0L -> TurnRobotLeft()
            1L -> TurnRobotRight()
        }
        robotPosition += robotDirection
    } while (!program.IsComplete())

    panelField.forEach {
        i -> for (j in i) {
            if (j == 0) print(ConsoleColors.BLACK_BACKGROUND + " ")
            else print(ConsoleColors.WHITE_BACKGROUND + "#")
        }
        println()
    }
    println("Answer: ${paintedPositions.size}")
}

fun SetupPanelField()
{
    for (y in 0..panelHeight)
    {
        panelField.add(mutableListOf())
        for (x in 0..panelWidth)
            panelField.last().add(0)
    }
}

fun GetPanelColorAtRobotPosition() : Int
{
    return panelField[robotPosition.y][robotPosition.x]
}

fun TurnRobotLeft()
{
    robotDirection = IntVector2(robotDirection.y, -robotDirection.x)
}

fun TurnRobotRight()
{
    robotDirection = IntVector2(-robotDirection.y, robotDirection.x)
}
