# AdventOfCode2019-Kotlin

This repository implements solutions in Kotlin to problems as given by Advent of Code 2019: https://adventofcode.com/2019

Code solutions are found in the `src` folder.
